exports.paymentState = {
    "paymentMethods": [
        "CREDIT_CARD",
        "REGISTERED_CARD",
        "E_BANKING",
        "PAYMENT_SLIP",
        "PAYMENT_SERVICE"
    ],
    "paymentProvider": {
        "merchantId": "1100005099",
        "providerUrl": "https://pilot.datatrans.biz",
        "sign": "37ce5f0288a6ec30d09ff72c8bbb0579757485b01ed65ad23f6e995fc98a5c6c",
        "amount": 0,
        "referenceNumber": "000000730701227700000000007",
        "currency": "CHF",
        "uppReturnMaskedCc": "true",
        "paymentCards": [
            "ECA",
            "VIS",
            "AMX",
            "PEF",
            "PFC"
        ],
        "paymentServices": [
            "PAP"
        ],
        "__typename": "PaymentProvider"
    },
    "registeredCards": [],
    "__typename": "PaymentState"
}