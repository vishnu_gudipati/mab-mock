exports.optionStatus=[
    {
      "options": [],
      "futureOptions": [
        {
          "id": "travel-day-pass-data",
          "name": "travel day pass data",
          "title": "travel day pass data",
          "subTitle": "100 MB",
          "description": "100 MB in Region 1 valid for 24 hours. You will be charged once you start using data abroad. Any existing roaming data options or packs will be used up first. ",
          "type": "DAY_PASS",
          "regions": [
            "ONE"
          ],
          "countries": [
            "Andorra",
            "Austria",
            "Belgium",
            "Bulgaria",
            "Canada",
            "Croatia",
            "Cyprus",
            "Czech Republic",
            "Denmark",
            "Estonia",
            "Faeroe Islands",
            "Finland",
            "France",
            "Germany",
            "Gibraltar",
            "Greece",
            "Greenland",
            "Guernsey",
            "Hungary",
            "Iceland",
            "Ireland",
            "Isle of Man",
            "Italy",
            "Jersey",
            "Latvia",
            "Liechtenstein",
            "Lithuania",
            "Luxembourg",
            "Malta",
            "Monaco",
            "Netherlands",
            "Norway",
            "Poland",
            "Portugal",
            "Puerto Rico",
            "Romania",
            "San Marino",
            "Slovakia",
            "Slovenia",
            "Spain",
            "Svalbard and Jan Mayen",
            "Sweden",
            "Turkey",
            "US Virgin Islands",
            "United Kingdom",
            "United States",
            "Vatican City",
            "Åland Islands"
          ],
          "header": "Valid 24 hours",
          "price": 1.9,
          "displayPriority": 100,
          "conflictsWith": null,
          "dependsOn": null,
          "status": "INSTALLED",
          "activatedAt": "2020-09-20T17:38:24+02:00",
          "expiresAt": null,
          "modifiable": false,
          "earliestDeactivationAt": "2021-04-26T12:20:00.191+02:00",
          "factSheet": {
            "name": "Factsheet (PDF)",
            "url": "/medias/sys_master/Factsheets/Factsheets/h17/hc0/8854179151902/M45-Factsheet-travel-day-pass-data-EN.pdf",
            "__typename": "FactSheet"
          },
          "buckets": [
            {
              "name": null,
              "active": false,
              "activatedAt": "2020-09-20T17:38:24+02:00",
              "expiresAt": null,
              "initial": 100,
              "available": 100,
              "measureUnit": "MB",
              "credit": "DATA",
              "type": "LIMITED",
              "__typename": "Bucket"
            }
          ],
          "__typename": "Option"
        }
      ],
      "autoInstalled": [],
      "__typename": "RegionStatus"
    }
  ]