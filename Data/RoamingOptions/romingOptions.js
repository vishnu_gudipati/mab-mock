const optionStatus = require('./optionStatus');
const settings = require('./roamingOptionSettings');
exports.subscriber= {
        "msisdn": "0765816111",
        "status": optionStatus.optionStatus,
        "segment": "SOHO",
        "settings" : settings.settings,
        "service": {
          "name": "We Mobile M",
          "type": "POSTPAID",
          "balance": null,
          "__typename": "ServicePlan"
        },
        "__typename": "Subscriber"
    }
  