exports.settings = {
        "cost": 0,
        "redirectToMailBoxAbroad": {
          "value": "true",
          "modifiable": true,
          "__typename": "Setting"
        },
        "callsAbroad": null,
        "seaAndAirSatelliteTraffic": {
          "value": "true",
          "modifiable": true,
          "__typename": "Setting"
        },
        "noDataRoamingOutsideBucket": {
          "value": "true",
          "modifiable": true,
          "__typename": "Setting"
        },
        "roamingInfoSms": null,
        "autoInstallRoamingOptions": null,
        "monthlyLimit": {
          "value": "100",
          "modifiable": true,
          "__typename": "Setting"
        },
        "__typename": "Settings"
      }