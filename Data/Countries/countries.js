exports.countries = {
	"countries": [{
		"isocode": "CH",
		"name": "Switzerland",
		"__typename": "Country"
	},
	{
		"isocode": "DE",
		"name": "Germany",
		"__typename": "Country"
	},
	{
		"isocode": "FR",
		"name": "France",
		"__typename": "Country"
	},
	{
		"isocode": "IT",
		"name": "Italy",
		"__typename": "Country"
	},
	{
		"isocode": "AT",
		"name": "Austria",
		"__typename": "Country"
	},
	{
		"isocode": "AF",
		"name": "Afghanistan",
		"__typename": "Country"
	},
	{
		"isocode": "AL",
		"name": "Albania",
		"__typename": "Country"
	},
	{
		"isocode": "DZ",
		"name": "Algeria",
		"__typename": "Country"
	},
	{
		"isocode": "AS",
		"name": "American Samoa",
		"__typename": "Country"
	},
	{
		"isocode": "AD",
		"name": "Andorra",
		"__typename": "Country"
	},
	{
		"isocode": "AO",
		"name": "Angola",
		"__typename": "Country"
	},
	{
		"isocode": "AI",
		"name": "Anguilla",
		"__typename": "Country"
	},
	{
		"isocode": "AQ",
		"name": "Antarctica",
		"__typename": "Country"
	},
	{
		"isocode": "AG",
		"name": "Antigua and Barbuda",
		"__typename": "Country"
	},
	{
		"isocode": "AR",
		"name": "Argentina",
		"__typename": "Country"
	},
	{
		"isocode": "AM",
		"name": "Armenia",
		"__typename": "Country"
	},
	{
		"isocode": "AW",
		"name": "Aruba",
		"__typename": "Country"
	},
	{
		"isocode": "AU",
		"name": "Australia",
		"__typename": "Country"
	},
	{
		"isocode": "AT",
		"name": "Austria",
		"__typename": "Country"
	},
	{
		"isocode": "AZ",
		"name": "Azerbaijan",
		"__typename": "Country"
	},
	{
		"isocode": "BS",
		"name": "Bahamas",
		"__typename": "Country"
	},
	{
		"isocode": "BH",
		"name": "Bahrain",
		"__typename": "Country"
	},
	{
		"isocode": "BD",
		"name": "Bangladesh",
		"__typename": "Country"
	},
	{
		"isocode": "BB",
		"name": "Barbados",
		"__typename": "Country"
	},
	{
		"isocode": "BY",
		"name": "Belarus",
		"__typename": "Country"
	},
	{
		"isocode": "BE",
		"name": "Belgium",
		"__typename": "Country"
	},
	{
		"isocode": "BZ",
		"name": "Belize",
		"__typename": "Country"
	},
	{
		"isocode": "BJ",
		"name": "Benin",
		"__typename": "Country"
	},
	{
		"isocode": "BM",
		"name": "Bermuda",
		"__typename": "Country"
	},
	{
		"isocode": "BT",
		"name": "Bhutan",
		"__typename": "Country"
	},
	{
		"isocode": "BO",
		"name": "Bolivia (Plurinational State of)",
		"__typename": "Country"
	},
	{
		"isocode": "BQ",
		"name": "Bonaire, Sint Eustatius and Saba",
		"__typename": "Country"
	},
	{
		"isocode": "BA",
		"name": "Bosnia and Herzegovina",
		"__typename": "Country"
	},
	{
		"isocode": "BW",
		"name": "Botswana",
		"__typename": "Country"
	},
	{
		"isocode": "BV",
		"name": "Bouvet Island",
		"__typename": "Country"
	},
	{
		"isocode": "BR",
		"name": "Brazil",
		"__typename": "Country"
	},
	{
		"isocode": "IO",
		"name": "British Indian Ocean Territory",
		"__typename": "Country"
	},
	{
		"isocode": "BN",
		"name": "Brunei Darussalam",
		"__typename": "Country"
	},
	{
		"isocode": "BG",
		"name": "Bulgaria",
		"__typename": "Country"
	},
	{
		"isocode": "BF",
		"name": "Burkina Faso",
		"__typename": "Country"
	},
	{
		"isocode": "BI",
		"name": "Burundi",
		"__typename": "Country"
	},
	{
		"isocode": "CV",
		"name": "Cabo Verde",
		"__typename": "Country"
	},
	{
		"isocode": "KH",
		"name": "Cambodia",
		"__typename": "Country"
	},
	{
		"isocode": "CM",
		"name": "Cameroon",
		"__typename": "Country"
	},
	{
		"isocode": "CA",
		"name": "Canada",
		"__typename": "Country"
	},
	{
		"isocode": "KY",
		"name": "Cayman Islands",
		"__typename": "Country"
	},
	{
		"isocode": "CF",
		"name": "Central African Republic",
		"__typename": "Country"
	},
	{
		"isocode": "TD",
		"name": "Chad",
		"__typename": "Country"
	},
	{
		"isocode": "CL",
		"name": "Chile",
		"__typename": "Country"
	},
	{
		"isocode": "CN",
		"name": "China",
		"__typename": "Country"
	},
	{
		"isocode": "CX",
		"name": "Christmas Island",
		"__typename": "Country"
	},
	{
		"isocode": "CC",
		"name": "Cocos (Keeling) Islands",
		"__typename": "Country"
	},
	{
		"isocode": "CO",
		"name": "Colombia",
		"__typename": "Country"
	},
	{
		"isocode": "KM",
		"name": "Comoros",
		"__typename": "Country"
	},
	{
		"isocode": "CG",
		"name": "Congo",
		"__typename": "Country"
	},
	{
		"isocode": "CD",
		"name": "Congo (Democratic Republic of the)",
		"__typename": "Country"
	},
	{
		"isocode": "CK",
		"name": "Cook Islands",
		"__typename": "Country"
	},
	{
		"isocode": "CR",
		"name": "Costa Rica",
		"__typename": "Country"
	},
	{
		"isocode": "HR",
		"name": "Croatia",
		"__typename": "Country"
	},
	{
		"isocode": "CU",
		"name": "Cuba",
		"__typename": "Country"
	},
	{
		"isocode": "CW",
		"name": "Cura�ao",
		"__typename": "Country"
	},
	{
		"isocode": "CY",
		"name": "Cyprus",
		"__typename": "Country"
	},
	{
		"isocode": "CZ",
		"name": "Czechia",
		"__typename": "Country"
	},
	{
		"isocode": "CI",
		"name": "C�te d'Ivoire",
		"__typename": "Country"
	},
	{
		"isocode": "DK",
		"name": "Denmark",
		"__typename": "Country"
	},
	{
		"isocode": "DJ",
		"name": "Djibouti",
		"__typename": "Country"
	},
	{
		"isocode": "DM",
		"name": "Dominica",
		"__typename": "Country"
	},
	{
		"isocode": "DO",
		"name": "Dominican Republic",
		"__typename": "Country"
	},
	{
		"isocode": "EC",
		"name": "Ecuador",
		"__typename": "Country"
	},
	{
		"isocode": "EG",
		"name": "Egypt",
		"__typename": "Country"
	},
	{
		"isocode": "SV",
		"name": "El Salvador",
		"__typename": "Country"
	},
	{
		"isocode": "GQ",
		"name": "Equatorial Guinea",
		"__typename": "Country"
	},
	{
		"isocode": "ER",
		"name": "Eritrea",
		"__typename": "Country"
	},
	{
		"isocode": "EE",
		"name": "Estonia",
		"__typename": "Country"
	},
	{
		"isocode": "SZ",
		"name": "Eswatini",
		"__typename": "Country"
	},
	{
		"isocode": "ET",
		"name": "Ethiopia",
		"__typename": "Country"
	},
	{
		"isocode": "FK",
		"name": "Falkland Islands (Malvinas)",
		"__typename": "Country"
	},
	{
		"isocode": "FO",
		"name": "Faroe Islands",
		"__typename": "Country"
	},
	{
		"isocode": "FJ",
		"name": "Fiji",
		"__typename": "Country"
	},
	{
		"isocode": "FI",
		"name": "Finland",
		"__typename": "Country"
	},
	{
		"isocode": "FR",
		"name": "France",
		"__typename": "Country"
	},
	{
		"isocode": "GF",
		"name": "French Guiana",
		"__typename": "Country"
	},
	{
		"isocode": "PF",
		"name": "French Polynesia",
		"__typename": "Country"
	},
	{
		"isocode": "TF",
		"name": "French Southern Territories",
		"__typename": "Country"
	},
	{
		"isocode": "GA",
		"name": "Gabon",
		"__typename": "Country"
	},
	{
		"isocode": "GM",
		"name": "Gambia",
		"__typename": "Country"
	},
	{
		"isocode": "GE",
		"name": "Georgia",
		"__typename": "Country"
	},
	{
		"isocode": "DE",
		"name": "Germany",
		"__typename": "Country"
	},
	{
		"isocode": "GH",
		"name": "Ghana",
		"__typename": "Country"
	},
	{
		"isocode": "GI",
		"name": "Gibraltar",
		"__typename": "Country"
	},
	{
		"isocode": "GR",
		"name": "Greece",
		"__typename": "Country"
	},
	{
		"isocode": "GL",
		"name": "Greenland",
		"__typename": "Country"
	},
	{
		"isocode": "GD",
		"name": "Grenada",
		"__typename": "Country"
	},
	{
		"isocode": "GP",
		"name": "Guadeloupe",
		"__typename": "Country"
	},
	{
		"isocode": "GU",
		"name": "Guam",
		"__typename": "Country"
	},
	{
		"isocode": "GT",
		"name": "Guatemala",
		"__typename": "Country"
	},
	{
		"isocode": "GG",
		"name": "Guernsey",
		"__typename": "Country"
	},
	{
		"isocode": "GN",
		"name": "Guinea",
		"__typename": "Country"
	},
	{
		"isocode": "GW",
		"name": "Guinea-Bissau",
		"__typename": "Country"
	},
	{
		"isocode": "GY",
		"name": "Guyana",
		"__typename": "Country"
	},
	{
		"isocode": "HT",
		"name": "Haiti",
		"__typename": "Country"
	},
	{
		"isocode": "HM",
		"name": "Heard Island and McDonald Islands",
		"__typename": "Country"
	},
	{
		"isocode": "VA",
		"name": "Holy See",
		"__typename": "Country"
	},
	{
		"isocode": "HN",
		"name": "Honduras",
		"__typename": "Country"
	},
	{
		"isocode": "HK",
		"name": "Hong Kong",
		"__typename": "Country"
	},
	{
		"isocode": "HU",
		"name": "Hungary",
		"__typename": "Country"
	},
	{
		"isocode": "IS",
		"name": "Iceland",
		"__typename": "Country"
	},
	{
		"isocode": "IN",
		"name": "India",
		"__typename": "Country"
	},
	{
		"isocode": "ID",
		"name": "Indonesia",
		"__typename": "Country"
	},
	{
		"isocode": "IR",
		"name": "Iran (Islamic Republic of)",
		"__typename": "Country"
	},
	{
		"isocode": "IQ",
		"name": "Iraq",
		"__typename": "Country"
	},
	{
		"isocode": "IE",
		"name": "Ireland",
		"__typename": "Country"
	},
	{
		"isocode": "IM",
		"name": "Isle of Man",
		"__typename": "Country"
	},
	{
		"isocode": "IL",
		"name": "Israel",
		"__typename": "Country"
	},
	{
		"isocode": "IT",
		"name": "Italy",
		"__typename": "Country"
	},
	{
		"isocode": "JM",
		"name": "Jamaica",
		"__typename": "Country"
	},
	{
		"isocode": "JP",
		"name": "Japan",
		"__typename": "Country"
	},
	{
		"isocode": "JE",
		"name": "Jersey",
		"__typename": "Country"
	},
	{
		"isocode": "JO",
		"name": "Jordan",
		"__typename": "Country"
	},
	{
		"isocode": "KZ",
		"name": "Kazakhstan",
		"__typename": "Country"
	},
	{
		"isocode": "KE",
		"name": "Kenya",
		"__typename": "Country"
	},
	{
		"isocode": "KI",
		"name": "Kiribati",
		"__typename": "Country"
	},
	{
		"isocode": "KP",
		"name": "Korea (Democratic People's Republic of)",
		"__typename": "Country"
	},
	{
		"isocode": "KR",
		"name": "Korea (Republic of)",
		"__typename": "Country"
	},
	{
		"isocode": "KW",
		"name": "Kuwait",
		"__typename": "Country"
	},
	{
		"isocode": "KG",
		"name": "Kyrgyzstan",
		"__typename": "Country"
	},
	{
		"isocode": "LA",
		"name": "Lao People's Democratic Republic",
		"__typename": "Country"
	},
	{
		"isocode": "LV",
		"name": "Latvia",
		"__typename": "Country"
	},
	{
		"isocode": "LB",
		"name": "Lebanon",
		"__typename": "Country"
	},
	{
		"isocode": "LS",
		"name": "Lesotho",
		"__typename": "Country"
	},
	{
		"isocode": "LR",
		"name": "Liberia",
		"__typename": "Country"
	},
	{
		"isocode": "LY",
		"name": "Libya",
		"__typename": "Country"
	},
	{
		"isocode": "LI",
		"name": "Liechtenstein",
		"__typename": "Country"
	},
	{
		"isocode": "LT",
		"name": "Lithuania",
		"__typename": "Country"
	},
	{
		"isocode": "LU",
		"name": "Luxembourg",
		"__typename": "Country"
	},
	{
		"isocode": "MO",
		"name": "Macao",
		"__typename": "Country"
	},
	{
		"isocode": "MK",
		"name": "Macedonia (the former Yugoslav Republic of)",
		"__typename": "Country"
	},
	{
		"isocode": "MG",
		"name": "Madagascar",
		"__typename": "Country"
	},
	{
		"isocode": "MW",
		"name": "Malawi",
		"__typename": "Country"
	},
	{
		"isocode": "MY",
		"name": "Malaysia",
		"__typename": "Country"
	},
	{
		"isocode": "MV",
		"name": "Maldives",
		"__typename": "Country"
	},
	{
		"isocode": "ML",
		"name": "Mali",
		"__typename": "Country"
	},
	{
		"isocode": "MT",
		"name": "Malta",
		"__typename": "Country"
	},
	{
		"isocode": "MH",
		"name": "Marshall Islands",
		"__typename": "Country"
	},
	{
		"isocode": "MQ",
		"name": "Martinique",
		"__typename": "Country"
	},
	{
		"isocode": "MR",
		"name": "Mauritania",
		"__typename": "Country"
	},
	{
		"isocode": "MU",
		"name": "Mauritius",
		"__typename": "Country"
	},
	{
		"isocode": "YT",
		"name": "Mayotte",
		"__typename": "Country"
	},
	{
		"isocode": "MX",
		"name": "Mexico",
		"__typename": "Country"
	},
	{
		"isocode": "FM",
		"name": "Micronesia (Federated States of)",
		"__typename": "Country"
	},
	{
		"isocode": "MD",
		"name": "Moldova (Republic of)",
		"__typename": "Country"
	},
	{
		"isocode": "MC",
		"name": "Monaco",
		"__typename": "Country"
	},
	{
		"isocode": "MN",
		"name": "Mongolia",
		"__typename": "Country"
	},
	{
		"isocode": "ME",
		"name": "Montenegro",
		"__typename": "Country"
	},
	{
		"isocode": "MS",
		"name": "Montserrat",
		"__typename": "Country"
	},
	{
		"isocode": "MA",
		"name": "Morocco",
		"__typename": "Country"
	},
	{
		"isocode": "MZ",
		"name": "Mozambique",
		"__typename": "Country"
	},
	{
		"isocode": "MM",
		"name": "Myanmar",
		"__typename": "Country"
	},
	{
		"isocode": "NA",
		"name": "Namibia",
		"__typename": "Country"
	},
	{
		"isocode": "NR",
		"name": "Nauru",
		"__typename": "Country"
	},
	{
		"isocode": "NP",
		"name": "Nepal",
		"__typename": "Country"
	},
	{
		"isocode": "NL",
		"name": "Netherlands",
		"__typename": "Country"
	},
	{
		"isocode": "NC",
		"name": "New Caledonia",
		"__typename": "Country"
	},
	{
		"isocode": "NZ",
		"name": "New Zealand",
		"__typename": "Country"
	},
	{
		"isocode": "NI",
		"name": "Nicaragua",
		"__typename": "Country"
	},
	{
		"isocode": "NE",
		"name": "Niger",
		"__typename": "Country"
	},
	{
		"isocode": "NG",
		"name": "Nigeria",
		"__typename": "Country"
	},
	{
		"isocode": "NU",
		"name": "Niue",
		"__typename": "Country"
	},
	{
		"isocode": "NF",
		"name": "Norfolk Island",
		"__typename": "Country"
	},
	{
		"isocode": "MP",
		"name": "Northern Mariana Islands",
		"__typename": "Country"
	},
	{
		"isocode": "NO",
		"name": "Norway",
		"__typename": "Country"
	},
	{
		"isocode": "OM",
		"name": "Oman",
		"__typename": "Country"
	},
	{
		"isocode": "PK",
		"name": "Pakistan",
		"__typename": "Country"
	},
	{
		"isocode": "PW",
		"name": "Palau",
		"__typename": "Country"
	},
	{
		"isocode": "PS",
		"name": "Palestine, State of",
		"__typename": "Country"
	},
	{
		"isocode": "PA",
		"name": "Panama",
		"__typename": "Country"
	},
	{
		"isocode": "PG",
		"name": "Papua New Guinea",
		"__typename": "Country"
	},
	{
		"isocode": "PY",
		"name": "Paraguay",
		"__typename": "Country"
	},
	{
		"isocode": "PE",
		"name": "Peru",
		"__typename": "Country"
	},
	{
		"isocode": "PH",
		"name": "Philippines",
		"__typename": "Country"
	},
	{
		"isocode": "PN",
		"name": "Pitcairn",
		"__typename": "Country"
	},
	{
		"isocode": "PL",
		"name": "Poland",
		"__typename": "Country"
	},
	{
		"isocode": "PT",
		"name": "Portugal",
		"__typename": "Country"
	},
	{
		"isocode": "PR",
		"name": "Puerto Rico",
		"__typename": "Country"
	},
	{
		"isocode": "QA",
		"name": "Qatar",
		"__typename": "Country"
	},
	{
		"isocode": "RO",
		"name": "Romania",
		"__typename": "Country"
	},
	{
		"isocode": "RU",
		"name": "Russian Federation",
		"__typename": "Country"
	},
	{
		"isocode": "RW",
		"name": "Rwanda",
		"__typename": "Country"
	},
	{
		"isocode": "RE",
		"name": "R�union",
		"__typename": "Country"
	},
	{
		"isocode": "BL",
		"name": "Saint Barth�lemy",
		"__typename": "Country"
	},
	{
		"isocode": "SH",
		"name": "Saint Helena, Ascension and Tristan da Cunha",
		"__typename": "Country"
	},
	{
		"isocode": "KN",
		"name": "Saint Kitts and Nevis",
		"__typename": "Country"
	},
	{
		"isocode": "LC",
		"name": "Saint Lucia",
		"__typename": "Country"
	},
	{
		"isocode": "MF",
		"name": "Saint Martin (French part)",
		"__typename": "Country"
	},
	{
		"isocode": "PM",
		"name": "Saint Pierre and Miquelon",
		"__typename": "Country"
	},
	{
		"isocode": "VC",
		"name": "Saint Vincent and the Grenadines",
		"__typename": "Country"
	},
	{
		"isocode": "WS",
		"name": "Samoa",
		"__typename": "Country"
	},
	{
		"isocode": "SM",
		"name": "San Marino",
		"__typename": "Country"
	},
	{
		"isocode": "ST",
		"name": "Sao Tome and Principe",
		"__typename": "Country"
	},
	{
		"isocode": "SA",
		"name": "Saudi Arabia",
		"__typename": "Country"
	},
	{
		"isocode": "SN",
		"name": "Senegal",
		"__typename": "Country"
	},
	{
		"isocode": "RS",
		"name": "Serbia",
		"__typename": "Country"
	},
	{
		"isocode": "SC",
		"name": "Seychelles",
		"__typename": "Country"
	},
	{
		"isocode": "SL",
		"name": "Sierra Leone",
		"__typename": "Country"
	},
	{
		"isocode": "SG",
		"name": "Singapore",
		"__typename": "Country"
	},
	{
		"isocode": "SX",
		"name": "Sint Maarten (Dutch part)",
		"__typename": "Country"
	},
	{
		"isocode": "SK",
		"name": "Slovakia",
		"__typename": "Country"
	},
	{
		"isocode": "SI",
		"name": "Slovenia",
		"__typename": "Country"
	},
	{
		"isocode": "SB",
		"name": "Solomon Islands",
		"__typename": "Country"
	},
	{
		"isocode": "SO",
		"name": "Somalia",
		"__typename": "Country"
	},
	{
		"isocode": "ZA",
		"name": "South Africa",
		"__typename": "Country"
	},
	{
		"isocode": "GS",
		"name": "South Georgia and the South Sandwich Islands",
		"__typename": "Country"
	},
	{
		"isocode": "SS",
		"name": "South Sudan",
		"__typename": "Country"
	},
	{
		"isocode": "ES",
		"name": "Spain",
		"__typename": "Country"
	},
	{
		"isocode": "LK",
		"name": "Sri Lanka",
		"__typename": "Country"
	},
	{
		"isocode": "SD",
		"name": "Sudan",
		"__typename": "Country"
	},
	{
		"isocode": "SR",
		"name": "Suriname",
		"__typename": "Country"
	},
	{
		"isocode": "SJ",
		"name": "Svalbard and Jan Mayen",
		"__typename": "Country"
	},
	{
		"isocode": "SE",
		"name": "Sweden",
		"__typename": "Country"
	},
	{
		"isocode": "CH",
		"name": "Switzerland",
		"__typename": "Country"
	},
	{
		"isocode": "SY",
		"name": "Syrian Arab Republic",
		"__typename": "Country"
	},
	{
		"isocode": "TW",
		"name": "Taiwan, Province of China",
		"__typename": "Country"
	},
	{
		"isocode": "TJ",
		"name": "Tajikistan",
		"__typename": "Country"
	},
	{
		"isocode": "TZ",
		"name": "Tanzania, United Republic of",
		"__typename": "Country"
	},
	{
		"isocode": "TH",
		"name": "Thailand",
		"__typename": "Country"
	},
	{
		"isocode": "TL",
		"name": "Timor-Leste",
		"__typename": "Country"
	},
	{
		"isocode": "TG",
		"name": "Togo",
		"__typename": "Country"
	},
	{
		"isocode": "TK",
		"name": "Tokelau",
		"__typename": "Country"
	},
	{
		"isocode": "TO",
		"name": "Tonga",
		"__typename": "Country"
	},
	{
		"isocode": "TT",
		"name": "Trinidad and Tobago",
		"__typename": "Country"
	},
	{
		"isocode": "TN",
		"name": "Tunisia",
		"__typename": "Country"
	},
	{
		"isocode": "TR",
		"name": "Turkey",
		"__typename": "Country"
	},
	{
		"isocode": "TM",
		"name": "Turkmenistan",
		"__typename": "Country"
	},
	{
		"isocode": "TC",
		"name": "Turks and Caicos Islands",
		"__typename": "Country"
	},
	{
		"isocode": "TV",
		"name": "Tuvalu",
		"__typename": "Country"
	},
	{
		"isocode": "UG",
		"name": "Uganda",
		"__typename": "Country"
	},
	{
		"isocode": "UA",
		"name": "Ukraine",
		"__typename": "Country"
	},
	{
		"isocode": "AE",
		"name": "United Arab Emirates",
		"__typename": "Country"
	},
	{
		"isocode": "GB",
		"name": "United Kingdom of Great Britain and Northern Ireland",
		"__typename": "Country"
	},
	{
		"isocode": "UM",
		"name": "United States Minor Outlying Islands",
		"__typename": "Country"
	},
	{
		"isocode": "US",
		"name": "United States of America",
		"__typename": "Country"
	},
	{
		"isocode": "UY",
		"name": "Uruguay",
		"__typename": "Country"
	},
	{
		"isocode": "UZ",
		"name": "Uzbekistan",
		"__typename": "Country"
	},
	{
		"isocode": "VU",
		"name": "Vanuatu",
		"__typename": "Country"
	},
	{
		"isocode": "VE",
		"name": "Venezuela (Bolivarian Republic of)",
		"__typename": "Country"
	},
	{
		"isocode": "VN",
		"name": "Viet Nam",
		"__typename": "Country"
	},
	{
		"isocode": "VG",
		"name": "Virgin Islands (British)",
		"__typename": "Country"
	},
	{
		"isocode": "VI",
		"name": "Virgin Islands (U.S.)",
		"__typename": "Country"
	},
	{
		"isocode": "WF",
		"name": "Wallis and Futuna",
		"__typename": "Country"
	},
	{
		"isocode": "EH",
		"name": "Western Sahara",
		"__typename": "Country"
	},
	{
		"isocode": "YE",
		"name": "Yemen",
		"__typename": "Country"
	},
	{
		"isocode": "ZM",
		"name": "Zambia",
		"__typename": "Country"
	},
	{
		"isocode": "ZW",
		"name": "Zimbabwe",
		"__typename": "Country"
	},
	{
		"isocode": "AX",
		"name": "�land Islands",
		"__typename": "Country"
	}]
}