exports.contactInput = {
	"firstName": "Hasan",
	"lastName": "Unlu",
	"email": "clarifynoreply@sunrise.net",
	"emailConfirmed": true,
	"birthDate": "1969-07-01",
	"phone": "0326756880",
    "languageCode": "DE",
    "salutation":"MR.",
	"__typename": "UpdateContactInput"
  }
