exports.options =  [
	{
	  "activationDate": "2015-12-16",
	  "factSheet": null,
	  "oneLineTitle": "Mobile ID",
	  "buckets": [],
	  "categories": [
		{
		  "name": "Default Category",
		  "code": "DefaultCategory_TechnicalCategorization",
		  "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Default-Category/c/DefaultCategory_TechnicalCategorization",
		  "technicalCategory": true,
		  "tabCategory": false,
		  "__typename": "Category"
		},
		{
		  "name": "Mobile ID",
		  "code": "MobileID_MobileOptions_Options_SunriseCategorization",
		  "url": "/Sunrise-Categorization/Options/Mobile-Options/Mobile-ID/c/MobileID_MobileOptions_Options_SunriseCategorization",
		  "technicalCategory": false,
		  "tabCategory": false,
		  "__typename": "Category"
		},
		{
		  "name": "Mobile ID",
		  "code": "MobileID_res_mtv_smoho ",
		  "url": "/Mobile-ID/c/MobileID_res_mtv_smoho%20",
		  "technicalCategory": false,
		  "tabCategory": false,
		  "__typename": "Category"
		}
	  ],
	  "conflictsWithInstalled": [],
	  "conflictsWith": [],
	  "dependsOnNotInstalled": null,
	  "earliestDeactivationDate": "2021-05-05",
	  "id": "Secure Login (Mobile ID)",
	  "info": null,
	  "installed": true,
	  "modifiable": false,
	  "scheduledActivationDate": null,
	  "scheduledDeactivationDate": null,
	  "status": "RECURRING",
	  "insurancePurchase": null,
	  "price": {
		"originalPrice": {
		  "value": 0,
		  "__typename": "ProductOriginalPrice"
		},
		"discountedPrice": null,
		"recurring": {
		  "duration": 1,
		  "period": "MONTH",
		  "__typename": "Recurring"
		},
		"__typename": "ProductPrice"
	  },
	  "__typename": "Option"
	},
	{
	  "activationDate": "2018-04-23",
	  "factSheet": {
		"name": "Factsheet (PDF)",
		"url": "{host}/medias/sys_master/Factsheets/Factsheets/hda/h01/9047132930078/M20-Factsheet-Call-Protect-Option-DE.pdf",
		"__typename": "FactSheet"
	  },
	  "oneLineTitle": "Call Protect",
	  "buckets": [],
	  "categories": [
		{
		  "name": "Normal Options",
		  "code": "NormalOptions_OptionTypes_TechnicalCategorization",
		  "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Option-Types/Normal-Options/c/NormalOptions_OptionTypes_TechnicalCategorization",
		  "technicalCategory": true,
		  "tabCategory": false,
		  "__typename": "Category"
		},
		{
		  "name": "Call Insurance",
		  "code": "CallInsurance_MobilePostpaidOptions_Options_SunriseCategorization",
		  "url": "/Sunrise-Categorization/Options/Mobile-Postpaid-Options/Call-Insurance/c/CallInsurance_MobilePostpaidOptions_Options_SunriseCategorization",
		  "technicalCategory": false,
		  "tabCategory": false,
		  "__typename": "Category"
		},
		{
		  "name": "Default Category",
		  "code": "DefaultCategory_TechnicalCategorization",
		  "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Default-Category/c/DefaultCategory_TechnicalCategorization",
		  "technicalCategory": true,
		  "tabCategory": false,
		  "__typename": "Category"
		},
		{
		  "name": "Sunrise Call Protect",
		  "code": "SunriseCallProtect ",
		  "url": "/Sunrise-Call-Protect/c/SunriseCallProtect%20",
		  "technicalCategory": false,
		  "tabCategory": false,
		  "__typename": "Category"
		}
	  ],
	  "conflictsWithInstalled": [],
	  "conflictsWith": [],
	  "dependsOnNotInstalled": null,
	  "earliestDeactivationDate": "2021-05-31",
	  "id": "Sunrise call protect",
	  "info": null,
	  "installed": true,
	  "modifiable": false,
	  "scheduledActivationDate": null,
	  "scheduledDeactivationDate": null,
	  "status": "RECURRING",
	  "insurancePurchase": null,
	  "price": {
		"originalPrice": {
		  "value": 1,
		  "__typename": "ProductOriginalPrice"
		},
		"discountedPrice": null,
		"recurring": {
		  "duration": 1,
		  "period": "MONTH",
		  "__typename": "Recurring"
		},
		"__typename": "ProductPrice"
	  },
	  "__typename": "Option"
	}
  ]