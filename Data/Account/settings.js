exports.settings = {
    "singlePrivacySetting": false,
    "privacySettings": [
      {
        "name": "PROFILE_ANALYSIS",
        "value": true,
        "__typename": "PrivacySetting"
      },
      {
        "name": "PROFILE_OFFER",
        "value": true,
        "__typename": "PrivacySetting"
      },
      {
        "name": "PROCESSING_SMART_DATA",
        "value": false,
        "__typename": "PrivacySetting"
      },
      {
        "name": "MULTISOURCE_ADDRESS_USAGE",
        "value": false,
        "__typename": "PrivacySetting"
      },
      {
        "name": "SERVICES_AND_TECHNOLOGY",
        "value": false,
        "__typename": "PrivacySetting"
      },
      {
        "name": "CHANNEL_SMS",
        "value": true,
        "serviceSettings": [
          {
            "serviceId": "0764262535",
            "attributes": [
              {
                "name": "SMS_ALLOWED",
                "value": true,
                "__typename": "ServiceSettingAttribute"
              }
            ],
            "__typename": "ServiceSetting"
          },
          {
            "serviceId": "0764262535",
            "attributes": [
              {
                "name": "SMS_ALLOWED",
                "value": true,
                "__typename": "ServiceSettingAttribute"
              }
            ],
            "__typename": "ServiceSetting"
          }
        ],
        "__typename": "PrivacySetting"
      },
      {
        "name": "CHANEL_CALL",
        "value": true,
        "__typename": "PrivacySetting"
      },
      {
        "name": "CHANNEL_LETTER",
        "value": true,
        "__typename": "PrivacySetting"
      },
      {
        "name": "CHANNEL_WEBAPP",
        "value": true,
        "__typename": "PrivacySetting"
      },
      {
        "name": "CHANNEL_MESSENGER",
        "value": true,
        "__typename": "PrivacySetting"
      }
    ],
    "__typename": "AccountSettings"
  }