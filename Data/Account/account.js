const owners = require('./owners'),
    options = require('./options'),
    primayaddress = require('./primayaddress'),
    billinfo = require('./billinfo'),
    settings = require('./settings'),
    services = require('./services'),
    retentionProducts = require('./retentionProducts'),
    xofProgram = require('./xofprogram');

exports.account = {
    "id": "7307012277",
    "hasSunriseOne" : true,
    "owner": owners.owners,
    "options": options.options,
    "segment": "SOHO",
    "primaryAddress": primayaddress.primaryAddress,
    "billInfo": billinfo.billInfo,
    "services": services.services,
    "settings": settings.settings,
    "retentionProducts": retentionProducts.retentionProducts,
    "xofProgram": xofProgram.xofProgram,
    "__typename": "Account"
}