exports.retentionProducts = [{
		"serial": "0794591448",
		"name": "Sunrise flex 40",
		"price": 25.0,
		"ceaseReason": "RETENTION_CASE_NO_CEASE",
		"retentionOffers": [{
			"priority": 2,
			"expiryDate": "2020-02-18T12:17:21Z",
			"creationDate": "2020-02-14T12:17:21Z",
			"destinationProducts": [{
				"name": "Freedom europe&US",
				"price": 130.0,
				"action": "ADDED",
				"__typename": "DestinationProduct"
			}, {
				"name": "Sunrise flex 40",
				"price": 25.0,
				"action": "DELETED",
				"__typename": "DestinationProduct"
			}],
			"discounts": [{
				"name": "Retention Discount Mobile",
				"serviceDuration": 24,
				"percentage": null,
				"action": "ADDED",
				"price": 60.0,
				"discountedPrice": 70.0,
				"__typename": "RetentionOfferDiscount"
			}],
			"options": [],
			"devicePlans": [],
			"accessoryPlans": [],
			"creditNotes": [],
			"__typename": "RetentionOffer"
		}, {
			"priority": 1,
			"expiryDate": "2020-02-18T12:17:19Z",
			"creationDate": "2020-02-14T12:17:19Z",
			"destinationProducts": [{
				"name": "Sunrise flex 40",
				"price": 25.0,
				"action": "DELETED",
				"__typename": "DestinationProduct"
			}, {
				"name": "Freedom swiss neighbors",
				"price": 95.0,
				"action": "ADDED",
				"__typename": "DestinationProduct"
			}],
			"discounts": [{
				"name": "Retention Discount Mobile",
				"serviceDuration": 24,
				"percentage": 36.84,
				"action": "ADDED",
				"price": 35.0,
				"discountedPrice": 60.0,
				"__typename": "RetentionOfferDiscount"
			}],
			"options": [{
				"name": "Default Postpaid Roaming Refr",
				"serviceDuration": 0,
				"price": null,
				"action": "DELETED",
				"__typename": "RetentionOfferOption"
			}, {
				"name": "Default Res Roam neighbors",
				"serviceDuration": 0,
				"price": null,
				"action": "ADDED",
				"__typename": "RetentionOfferOption"
			}, {
				"name": "Default Postpaid International",
				"serviceDuration": 0,
				"price": null,
				"action": "DELETED",
				"__typename": "RetentionOfferOption"
			}, {
				"name": "Default Res Intl neighbors",
				"serviceDuration": 0,
				"price": null,
				"action": "ADDED",
				"__typename": "RetentionOfferOption"
			}, {
				"name": "Default data flex",
				"serviceDuration": 0,
				"price": null,
				"action": "DELETED",
				"__typename": "RetentionOfferOption"
			}, {
				"name": "RC OPSC - travel day pass data",
				"serviceDuration": 0,
				"price": null,
				"action": "ADDED",
				"__typename": "RetentionOfferOption"
			}],
			"devicePlans": [],
			"accessoryPlans": [],
			"creditNotes": [],
			"__typename": "RetentionOffer"
		}],
		"__typename": "RetentionProduct"
	}]