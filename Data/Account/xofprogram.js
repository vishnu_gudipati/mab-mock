exports.xofProgram = {
  "accountInfo": {
    "email": "",
    "companyName": "Relase Test EoF Mastersite",
    "companyId": "RELEA",
    "discountInfo": {
      "allowedDiscounts": 5,
      "remainingDiscounts": 3,
      "renewalDuration": 6,
      "duration": 12,
      "newExpiryDate": "2022-04-29",
      "discountPercentage": 20,
      "type": "RELEA",
      "__typename": "XofAccountDiscountInfo"
    },
    "__typename": "XofProgramAccountInfo"
  },
  "status": {
    "activationStatus": "ACTIVE",
    "type": "EOF",
    "__typename": "XofAccountProgramStatus"
  },
  "__typename": "XofAccountProgram"
}
  