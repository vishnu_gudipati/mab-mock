exports.primaryAddress =  {
    "salutation": "HERR",
    "countryIsocode": "CH",
    "countryName": "Switzerland",
    "name": "Hasan Unlu",
    "firstName": "Hasan",
    "lastName": "Unlu",
    "postalCode": "4563",
    "streetName": "Steinhölzlistrasse",
    "streetNumber": "5",
    "town": "Gerlafingen",
    "__typename": "Address"
  }