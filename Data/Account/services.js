const   options = require('./options');
exports.services = [
  {
    "id": "0765816132",
    "merge": false,
    "name": "GSM Contract",
    "ratePlans": [
      {
        "oneLineTitle": "We Mobile M",
        "name": "We Mobile M",
        "type": "MOBILE_POSTPAID",
        "options" :options,
        "buckets": [
          {
            "name": "Calling time",
            "type": "UNLIMITED",
            "credits": [
              "VOICE"
            ],
            "measureUnits": [],
            "group": "NATIONAL",
            "category": "VOICE",
            "showValidToCountdown": false,
            "__typename": "Bucket"
          },
          {
            "name": "SMS",
            "type": "UNLIMITED",
            "credits": [
              "SMS"
            ],
            "measureUnits": [],
            "group": "NATIONAL",
            "category": "MESSAGES",
            "showValidToCountdown": false,
            "__typename": "Bucket"
          },
          {
            "name": "High Speed Internet",
            "type": "UNLIMITED",
            "credits": [
              "DATA"
            ],
            "measureUnits": [],
            "group": "NATIONAL",
            "category": "MOBILE_DATA",
            "showValidToCountdown": false,
            "__typename": "Bucket"
          },
          {
            "name": "Calling time",
            "type": "NOT_INCLUDED",
            "credits": [
              "VOICE"
            ],
            "measureUnits": [],
            "group": "INTERNATIONAL",
            "category": "VOICE",
            "showValidToCountdown": false,
            "__typename": "Bucket"
          }
        ],
        "entitlements": [
          {
            "group": "WESurfingCallsSMSMMSCH",
            "value": "Unlimited calls, SMS/MMS within CH",
            "description": "<style>.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}</style>",
            "credits": [],
            "__typename": "Entitlement"
          },
          {
            "group": "WESurfingSpeed",
            "value": "Unlimited surfing with up to 100 Mbit/s in CH, 5G included",
            "credits": [],
            "__typename": "Entitlement"
          },
          {
            "group": "WeTVNeo_Mobile",
            "value": "TV neo light: TV for mobile devices & PCs",
            "description": "<style><!--\r\n.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}\r\n--></style><style><!--\r\n.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}\r\n--></style><style><!--\r\n.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}\r\n--></style><style>.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}</style>",
            "credits": [],
            "__typename": "Entitlement"
          }
        ],
        "categories": [
          {
            "name": "Default Category",
            "code": "DefaultCategory_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Default-Category/c/DefaultCategory_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "We MobilePostpaid Pack",
            "code": "WE_MobilePostpaid_Pack_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/We-MobilePostpaid-Pack/c/WE_MobilePostpaid_Pack_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Change Subscriber Section",
            "code": "ChangeSubscriber_ProductChangeActions_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Detail-Page-Structure/Change-Subscriber-Section/c/ChangeSubscriber_ProductChangeActions_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Add FSD Change",
            "code": "AddExtraSimChange_ProductChangeActions_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Product-Change-Actions/Add-FSD-Change/c/AddExtraSimChange_ProductChangeActions_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Buy new Mobile Device",
            "code": "BuynewMobileDevice_ProductChangeActions_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Product-Change-Actions/Buy-new-Mobile-Device/c/BuynewMobileDevice_ProductChangeActions_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Device Accordion",
            "code": "DeviceAccordion_DetailPageAccordionStructure_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Detail-Page-Accordion-Structure/Device-Accordion/c/DeviceAccordion_DetailPageAccordionStructure_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "OneDrive Options",
            "code": "OneDriveOptions_OptionTypes_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Option-Types/OneDrive-Options/c/OneDriveOptions_OptionTypes_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Roaming Data Bucket Options",
            "code": "RoamingDataBucketOptions_OptionTypes_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Option-Types/Roaming-Data-Bucket-Options/c/RoamingDataBucketOptions_OptionTypes_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Roaming Days Bucket Options",
            "code": "RoamingDaysBucketOptions_OptionTypes_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Option-Types/Roaming-Days-Bucket-Options/c/RoamingDaysBucketOptions_OptionTypes_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Device Insurance Options",
            "code": "DeviceInsuranceOptions_OptionTypes_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Option-Types/Device-Insurance-Options/c/DeviceInsuranceOptions_OptionTypes_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Buy new Mobile Device",
            "code": "BuynewMobileDevice_ProductChangeButtons_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Product-Change-Buttons/Buy-new-Mobile-Device/c/BuynewMobileDevice_ProductChangeButtons_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Mobile Postpaid Current",
            "code": "MobilePostpaidCurrent_DetailPageStructure_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Detail-Page-Structure/Mobile-Postpaid-Current/c/MobilePostpaidCurrent_DetailPageStructure_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Phone Number",
            "code": "PhoneNumber_DetailPageSubtitle_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Detail-Page-Subtitle/Phone-Number/c/PhoneNumber_DetailPageSubtitle_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Buy Accessories",
            "code": "BuyAccessories_ProductChangeActions_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Product-Change-Actions/Buy-Accessories/c/BuyAccessories_ProductChangeActions_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Running Cost Accordion",
            "code": "RunningCostAccordion_DetailPageAccordionStructure_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Detail-Page-Accordion-Structure/Running-Cost-Accordion/c/RunningCostAccordion_DetailPageAccordionStructure_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Options Accordion",
            "code": "OptionsAccordion_DetailPageAccordionStructure_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Detail-Page-Accordion-Structure/Options-Accordion/c/OptionsAccordion_DetailPageAccordionStructure_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "SIM Card Accordion",
            "code": "SIMCardAccordion_DetailPageAccordionStructure_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Detail-Page-Accordion-Structure/SIM-Card-Accordion/c/SIMCardAccordion_DetailPageAccordionStructure_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Subscription Detail Accordion",
            "code": "SubscriptionDetailAccordion_DetailPageAccordionStructure_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Detail-Page-Accordion-Structure/Subscription-Detail-Accordion/c/SubscriptionDetailAccordion_DetailPageAccordionStructure_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Speed Extra Options",
            "code": "SpeedExtraOptions_OptionTypes_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Option-Types/Speed-Extra-Options/c/SpeedExtraOptions_OptionTypes_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Buy Accessories",
            "code": "BuyAccessories_ProductChangeButtons_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Product-Change-Buttons/Buy-Accessories/c/BuyAccessories_ProductChangeButtons_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Cost Control Accordion",
            "code": "CostControlAccordion_DetailPageAccordionStructure_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Detail-Page-Accordion-Structure/Cost-Control-Accordion/c/CostControlAccordion_DetailPageAccordionStructure_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Blocking Sets Accordion",
            "code": "BlockingSetsAccordion_DetailPageAccordionStructure_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Detail-Page-Accordion-Structure/Blocking-Sets-Accordion/c/BlockingSetsAccordion_DetailPageAccordionStructure_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Roaming Settings Accordion",
            "code": "RoamingSettingsAccordion_DetailPageAccordionStructure_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Detail-Page-Accordion-Structure/Roaming-Settings-Accordion/c/RoamingSettingsAccordion_DetailPageAccordionStructure_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Roaming Options",
            "code": "RoamingOptions_OptionTypes_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Option-Types/Roaming-Options/c/RoamingOptions_OptionTypes_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Whitelisted Categories for SMALL Tenant",
            "code": "Filter_SMALL_AllowCategories",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/c/Filter_SMALL_AllowCategories",
            "technicalCategory": false,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Add FSD Change",
            "code": "AddExtraSimChange_ProductChangeButtons_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Product-Change-Buttons/Add-FSD-Change/c/AddExtraSimChange_ProductChangeButtons_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "code": "FriendsOfFirm_DetailPageAccordionStructure_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant//c/FriendsOfFirm_DetailPageAccordionStructure_TechnicalCategorization",
            "technicalCategory": false,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Private customers",
            "code": "RC_Segment_SunriseCategorization",
            "url": "/Sunrise-Categorization/Segment/Private-customers/c/RC_Segment_SunriseCategorization",
            "technicalCategory": false,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Tidal HiFi Option",
            "code": "TidalHiFiOptions_OptionTypes_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Option-Types/Tidal-HiFi-Option/c/TidalHiFiOptions_OptionTypes_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "One Mobile",
            "code": "OneMobile_OneRatePlan_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/One-Rate-Plan/One-Mobile/c/OneMobile_OneRatePlan_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "code": "AddExtraSimWatchChange_ProductChangeActions_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Product-Change-Actions//c/AddExtraSimWatchChange_ProductChangeActions_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "code": "AddExtraSimWatchChange_ProductChangeButtons_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Product-Change-Buttons//c/AddExtraSimWatchChange_ProductChangeButtons_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Display Owner Button",
            "code": "ChangeOwner_ProductChangeButtons_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Product-Change-Buttons/Display-Owner-Button/c/ChangeOwner_ProductChangeButtons_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Change Owner Button",
            "code": "ChangeOwner_ProductChangeActions_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Product-Change-Actions/Change-Owner-Button/c/ChangeOwner_ProductChangeActions_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Business SIM category",
            "code": "AddBusinessSimChange_ProductChangeButtons_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Business-SIM-category/c/AddBusinessSimChange_ProductChangeButtons_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Business SIM category",
            "code": "AddBusinessSimChange_ProductChangeActions_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Business-SIM-category/c/AddBusinessSimChange_ProductChangeActions_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "AddExtraSimPackageChange_ProductChangeButtons_TechnicalCategorization",
            "code": "AddExtraSimPackageChange_ProductChangeButtons_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Product-Change-Buttons/AddExtraSimPackageChange-ProductChangeButtons-TechnicalCategorization/c/AddExtraSimPackageChange_ProductChangeButtons_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "code": "DiscountsAccordion_DetailPageAccordionStructure_TechnicalCategorization",
            "url": "//c/DiscountsAccordion_DetailPageAccordionStructure_TechnicalCategorization",
            "technicalCategory": false,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "4G Compatible Product",
            "code": "Product_5G_Compatible_Categorization",
            "url": "/4G-Compatible-Product/c/Product_5G_Compatible_Categorization",
            "technicalCategory": false,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "4G Product",
            "code": "Product_4G_SunriseCategorization",
            "url": "/4G-Product/c/Product_4G_SunriseCategorization",
            "technicalCategory": false,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "XOF Accordion",
            "code": "XOFSection_DetailPageAccordionStructure_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/XOF-Accordion/c/XOFSection_DetailPageAccordionStructure_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Mobile Postpaid Change",
            "code": "MobilePostpaidChange_ProductChangeActions_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Product-Change-Actions/Mobile-Postpaid-Change/c/MobilePostpaidChange_ProductChangeActions_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Mobile Postpaid Change",
            "code": "MobilePostpaidChange_ProductChangeButtons_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Product-Change-Buttons/Mobile-Postpaid-Change/c/MobilePostpaidChange_ProductChangeButtons_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "Mobile Postpaid RatePlan SellingWithHandy TechnicalCategorization",
            "code": "MobilePostpaidRatePlans_SellingWithHandy_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/Mobile-Postpaid-RatePlan-SellingWithHandy-TechnicalCategorization/c/MobilePostpaidRatePlans_SellingWithHandy_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "We MobilePostpaid PackChange",
            "code": "WE_MobilePostpaidPack_ProductChangeActions_TechnicalCategorization",
            "url": "/Whitelisted-Categories-for-SMALL-Tenant/Technical-Categorization/We-MobilePostpaid-PackChange/c/WE_MobilePostpaidPack_ProductChangeActions_TechnicalCategorization",
            "technicalCategory": true,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "subscription only",
            "code": "WE_MobilePostpaid_tariff_TabCategorization",
            "url": "/Tab-Categorization/subscription-only/c/WE_MobilePostpaid_tariff_TabCategorization",
            "technicalCategory": false,
            "tabCategory": true,
            "deviceRestriction": false,
            "__typename": "Category"
          },
          {
            "name": "GIFT MAP for We Mobile M",
            "code": "WE_MobilePostpaid_GIFT_MAP_We Mobile M",
            "url": "/GIFT-MAP-for-We-Mobile-M/c/WE_MobilePostpaid_GIFT_MAP_We%20Mobile%20M",
            "technicalCategory": false,
            "tabCategory": false,
            "deviceRestriction": false,
            "__typename": "Category"
          }
        ],
        "lifecycle": "ACTIVE",
        "factSheet": {
          "name": "Factsheet (PDF)",
          "url": "{host}/medias/sys_master/Factsheets/Factsheets/h7e/h65/9181650059294/A41-We-Mobile-M-EN.pdf",
          "__typename": "FactSheet"
        },
        "__typename": "RatePlan"
      }
    ],
    "subscriber": {
      "firstName": "Susanne",
      "lastName": "Muster",
      "name": "Susanne Muster",
      "email": "ivo.stillhart@sunrise.net",
      "emailConfirmed": false,
      "birthDate": "1990-01-01",
      "__typename": "Subscriber"
    },
    "__typename": "Service"
  }
]