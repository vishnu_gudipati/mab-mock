exports.billInfo = {
    "totalOutstandingAmount": 0,
    "outstandingAmount": 0,
    "overdueAmount": 0,
    "bills": [
        {
            "id": "1045672169",
            "billingPeriodFrom": "2020-07-01",
            "billingPeriodTo": "2020-07-31",
            "dueOn": "2020-08-31",
            "billDate": "2020-08-01",
            "amount": 88,
            "openAmount": 0,
            "paid": true,
            "digitalInvoiceUrl": "{host}/sunrisecommercewebservices/v2/web/users/current/site/invoices/1045672169/digital",
            "status": "CLOSED",
            "ppaEligible": false,
            "__typename": "Bill"
        },
        {
            "id": "1026718199",
            "billingPeriodFrom": "2020-05-01",
            "billingPeriodTo": "2020-05-31",
            "dueOn": "2020-07-31",
            "billDate": "2020-06-01",
            "amount": 88,
            "openAmount": 0,
            "paid": true,
            "digitalInvoiceUrl": "{host}/sunrisecommercewebservices/v2/web/users/current/site/invoices/1026718199/digital",
            "status": "CLOSED",
            "ppaEligible": false,
            "__typename": "Bill"
        },
        {
            "id": "1036799570",
            "billingPeriodFrom": "2020-06-01",
            "billingPeriodTo": "2020-06-30",
            "dueOn": "2020-07-31",
            "billDate": "2020-07-01",
            "amount": 88.3,
            "openAmount": 0,
            "paid": true,
            "digitalInvoiceUrl": "{host}/sunrisecommercewebservices/v2/web/users/current/site/invoices/1036799570/digital",
            "status": "CLOSED",
            "ppaEligible": false,
            "__typename": "Bill"
        },
        {
            "id": "1009532785",
            "billingPeriodFrom": "2020-03-01",
            "billingPeriodTo": "2020-03-31",
            "dueOn": "2020-06-30",
            "billDate": "2020-04-01",
            "amount": 88,
            "openAmount": 0,
            "paid": true,
            "digitalInvoiceUrl": "{host}/sunrisecommercewebservices/v2/web/users/current/site/invoices/1009532785/digital",
            "status": "CLOSED",
            "ppaEligible": false,
            "__typename": "Bill"
        },
        {
            "id": "1019427244",
            "billingPeriodFrom": "2020-04-01",
            "billingPeriodTo": "2020-04-30",
            "dueOn": "2020-06-30",
            "billDate": "2020-05-01",
            "amount": 88.3,
            "openAmount": 0,
            "paid": true,
            "digitalInvoiceUrl": "{host}/sunrisecommercewebservices/v2/web/users/current/site/invoices/1019427244/digital",
            "status": "CLOSED",
            "ppaEligible": false,
            "__typename": "Bill"
        }
    ],
    "billingAddress": {
        "sameAsCustomerAddress": false,
        "differentBillingAddress": {
            "salutation": "HERR",
            "countryIsocode": "CH",
            "countryName": "Switzerland",
            "name": "Hasan Ünlü",
            "firstName": "Hasan",
            "lastName": "Ünlü",
            "postalCode": "4563",
            "streetName": "Steinhölzlistrasse",
            "streetNumber": "5",
            "town": "Gerlafingen",
            "__typename": "Address"
        },
        "__typename": "BillingAddress"
    },
    "billSettings": {
        "paymentMethod": "PAY_SLIP",
        "billDeliveryMethod": "EMAIL_WITH_BILL_IN_PDF_FORMAT",
        "billDeliveryNotification": {
            "emailAddress": "clarifynoreply@sunrise.net",
            "contactNumbers": [],
            "__typename": "BillDeliveryNotification"
        },
        "billAnonymizationLevel": "NONE",
        "__typename": "BillSettings"
    },
    "registeredCards": [],
    "ppaEligibility": {
        "eligible": false,
        "maxAmount": 5000,
        "minAmount": 100,
        "minInstallmentAmount": 50,
        "fee": 5,
        "minInstallments": 2,
        "maxInstallments": 8,
        "__typename": "PpaEligibility"
    },
    "dunningInfo": {
        "dunningJokerEligibility": true,
        "eligibileForDeblocking": false,
        "dunningLevel": null,
        "dunningHistory": null,
        "__typename": "DunningInfo"
    },
    "__typename": "BillInfo"
}