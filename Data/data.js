const account = require('./Account/account'),
	paymentState = require('./PaymentState/paymentstate'),
	paymentRequests = require('./paymentRequests/paymentRequests'),
	countries = require('./Countries/countries'),
	roamingOptions = require('./RoamingOptions/romingOptions'),
	emailValidation = require('./Account/emailValidate');

exports.data = {
	"account": account.account,
	"addressValidity": {
		"additional": null,
		"postalCode": "8802",
		"streetName": "Bungertstrasse",
		"streetNumber": "25",
		"town": "Kilchberg",
		"confidence": 100,
		"status": "SUGGESTION",
		"__typename": "AddressValidity"
	},
	"paymentState": paymentState.paymentState,
	"paymentRequests": paymentRequests.paymentRequests,
	"countries": countries.countries,
	"roamingOptions" : roamingOptions.subscriber,
	"emailValidation" : emailValidation.emailValidationState
}