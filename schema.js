
const SchemaDef = `
input AcceptRetentionOfferInput {
  serviceId: String!
  priority: Int!
}

type AcceptRetentionOfferPayload {
  payloadStatus: PayloadStatus!
}

type Account {
  id: ID!
  score: Int
  owner(forceUpdate: Boolean): Subscriber!
  services(serviceId: String): [Service]!
  billInfo: BillInfo
  primaryAddress: Address
  hasSunriseOne: Boolean!
  segment: Segment!
  relocationDetails: RelocationInfo
  settings: AccountSettings!
  options(category: SiteLevelOptionCategory): [SiteLevelOption]
  retentionProducts: [RetentionProduct]
  xofProgram: XofAccountProgram
  scanIdStatus: ScanIdStatus
}

type AccountSettings {
  singlePrivacySetting: Boolean
  privacySettings: [PrivacySetting!]
}

enum AccountStatementMethod {
  EMAIL
  POST
}

type Address {
  salutation: SalutationType
  countryIsocode: String
  countryName: String
  name: String
  firstName: String
  lastName: String
  postalCode: String
  streetName: String
  streetNumber: String
  town: String
}

input AddressInput {
  salutation: SalutationType
  countryIsocode: String
  firstName: String
  lastName: String
  postalCode: String
  streetName: String
  streetNumber: String
  town: String
}

type AddressResult {
  zip: String!
  city: String
  street: String
  houseNumber: String
}

input AddressSuggestionInput {
  zip: String
  city: String
  street: String
  houseNumber: String
}

type AddressSuggestionPayload {
  result: [AddressResult]
}

type AddressValidity {
  additional: String
  postalCode: String
  streetName: String
  streetNumber: String
  town: String
  confidence: Int
  status: AddressValidityStatus!
}

input AddressValidityInput {
  postalCode: String!
  streetName: String!
  streetNumber: String!
  town: String!
  additional: String
  minConfidence: Int
}

enum AddressValidityStatus {
  VALID
  INVALID
  SUGGESTION
}

type Agb {
  name: String
  fileName: String
  url: String
}

type Bill {
  id: String
  billingPeriodFrom: LocalDate
  billingPeriodTo: LocalDate
  dueOn: LocalDate
  billDate: LocalDate
  amount: Float
  openAmount: Float
  paid: Boolean
  digitalInvoiceUrl: String
  documentType: BillDocumentType
  status: BillStatus
  ppaEligible: Boolean!
  sapDocumentId: String
}

enum BillAnonymizationLevel {
  NONE
  LAST_4_DIGITS
  LAST_2_DIGITS
}

input BillAnonymizationLevelInput {
  billAnonymizationLevel: BillAnonymizationLevel!
}

type BillAnonymizationLevelPayload {
  payloadStatus: PayloadStatus
}

enum BillDeliveryMethod {
  EMAIL_WITH_LINK
  EMAIL_WITH_BILL_IN_PDF_FORMAT
  PAPER_BILL_WITHOUT_CALL_DETAILS
  PAPER_BILL_WITH_CALL_DETAILS
}

type BillDeliveryNotification {
  emailAddress: String
  contactNumbers: [String]
}

input BillDeliveryNotificationInput {
  emailAddress: String
  contactNumbers: [String]
}

enum BillDocumentType {
  B1
  YP
  YT
  YU
  YW
  CH
  YC
  MB
  IP
  RT
}

type BillInfo {
  outstandingAmount: Float
  totalOutstandingAmount: Float
  overdueAmount: Float
  bills: [Bill]
  dunningInfo: DunningInfo
  billingAddress: BillingAddress
  billSettings: BillSettings
  registeredCards: [RegisteredCard]
  ppaEligibility: PpaEligibility!
  paymentAmounts: [PaymentAmount]
}

type BillingAddress {
  sameAsCustomerAddress: Boolean
  differentBillingAddress: Address
}

type BillingTime {
  code: String
}

type BillSettings {
  paymentMethod: PaymentMethodType
  billDeliveryMethod: BillDeliveryMethod
  billDeliveryNotification: BillDeliveryNotification
  billAnonymizationLevel: BillAnonymizationLevel
}

enum BillStatus {
  NEW
  OPEN
  PARTIALY_CLOSED
  CLOSED
  IN_DISPUTE
  DUNNED
  OVERDUE
}

type Bucket {
  name: String
  type: BucketType
  initial: Float
  remaining: Float
  used: Float
  credits: [BucketCredit]
  measureUnits: [BucketMeasureUnit]
  validFrom: DateTime
  validTo: DateTime
  showValidToCountdown: Boolean
  category: BucketCategory
  group: GroupType
  customText: String
  lastUpdatedDate: DateTime
}

enum BucketCategory {
  MOBILE_DATA
  FIXNET
  VOICE
  TV
  MESSAGES
}

enum BucketCredit {
  DATA
  SMS
  MMS
  VOICE
  WHATSAPP
  SNAPCHAT
  APPLEMUSIC
  DATA_SPEED_EXTRA
}

enum BucketMeasureUnit {
  SECOND
  MINUTE
  VOICE
  CHF
  SMS
  MMS
  KB
  MB
  GB
  GBPS
  MBPS
  PIECE
}

enum BucketType {
  LIMITED
  UNLIMITED
  THROTTLED
  NOT_INCLUDED
}

type Category {
  code: String
  name: String
  url: String
  technicalCategory: Boolean
  tabCategory: Boolean
  deviceRestriction: Boolean
}

type CliNumber {
  number: String
  type: CliNumberType
}

enum CliNumberType {
  MAIN
  ADDITIONAL
}

type Contract {
  startDate: LocalDate
  endDate: LocalDate
  duration: Int
}

type Country {
  isocode: String
  name: String
}

type CreateXofProgramPayload {
  payloadStatus: PayloadStatus!
}

scalar DateTime

input DeleteRegisteredCardInput {
  registeredCard: RegisteredCardInput!
}

type DeleteRegisteredCardPayload {
  payloadStatus: PayloadStatus!
}

type DestinationProduct {
  name: String!
  price: Float!
  action: RetentionOfferItemAction!
}

type DunningHistoryEntry {
  level: Int
  date: LocalDate
}

type DunningInfo {
  dunningJokerEligibility: Boolean
  eligibileForDeblocking: Boolean
  dunningLevel: Int
  dunningHistory: [DunningHistoryEntry]
}

input EditInsuranceOptionEntry {
  optionId: String
  action: EditOptionAction
  deviceDescription: String
  devicePrice: Float
  deviceState: InsuranceOptionDeviceState
  imei: String
}

enum EditOptionAction {
  ACTIVATE
  DEACTIVATE
}

input EditOptionEntry {
  action: EditOptionAction
  optionId: String
  additionalProperties: [OptionAdditionalProperty]
}

input EditOptionsInput {
  serviceId: String
  ratePlanName: String
  ratePlanType: RatePlanType
  editOptionEntries: [EditOptionEntry]
  editInsuranceOptionEntries: [EditInsuranceOptionEntry]
}

type EditOptionsPayload {
  payloadStatus: PayloadStatus!
}

enum EmailValidationMessage {
  BAD_ADDRESS
  DOMAIN_NOT_QUALIFIED
  MX_LOOKUP_ERROR
  NO_REPLY_ADDRESS
  ADDRESS_REJECTED
  SERVER_UNAVAILABLE
  ADDRESS_UNAVAILABLE
  DOMAIN_MISSPELLED
  VALIDATION_DELAYED
  RATE_LIMIT_EXCEEDED
  API_KEY_INVALID
  TASK_ACCEPTED
  LOCAL_ADDRESS
  IP_ADDRESS_LITERAL
  DISPOSABLE_ADDRESS
  ROLE_ADDRESS
  DUPLICATE_ADDRESS
  SERVER_REJECT
  VALID_ADDRESS
  CATCH_ALL_ACTIVE
  CATCH_ALL_TEST_DELAYED
  VALIDATION_OFF
  REGULAR_EXP_ERROR
  TECHNICAL_ERROR
}

type EmailValidationState {
  status: EmailValidationStatus!
  detailMessage: EmailValidationMessage
}

enum EmailValidationStatus {
  VALID
  INVALID
}

input EnrollXofProgramInput {
  serviceId: String!
  email: String
  validationType: XofEmailValidationType!
}

type EnrollXofProgramPayload {
  payloadStatus: PayloadStatus!
}

type Entitlement {
  group: String
  groupName: String
  name: String
  value: String
  description: String
  credits: [BucketCredit]
}

type FactSheet {
  name: String
  url: String
}

type Fee {
  priceValue: Float
  currencyIso: String
}

enum FirmProgramType {
  EOF
  FOF
}

type FixnetInfo {
  downloadSpeed: Int
  uploadSpeed: Int
  measureUnit: BucketMeasureUnit
  downloadSpeedText: String
  uploadSpeedText: String
}

enum FixnetStatus {
  ACTIVE
  ACTIVE_INT_LOW
  INACTIVE
  SUSPENDED_BY_USER
  SUSPENDED_BY_OPERATOR
  DEACTIVE
  TERMINATED
}

enum GroupType {
  NATIONAL
  INTERNATIONAL
  ROAMING
}

type Hardware {
  description: String
  serialNumber: String
}

type HardwarePlan {
  sapInstallmentId: String!
  duration: Int!
  info: HardwarePlanInfo!
  totalPrice: Float!
  monthlyPayment: Float!
  upfrontPayment: Float!
  purchasePrice: Float
  type: HardwarePlanType
}

type HardwarePlanInfo {
  sapId: String!
  description: String
  serialNumber: String
}

enum HardwarePlanType {
  DEVICE
  ACCESSORY
}

enum InsuranceOptionDeviceState {
  NEW
  USED
}

type LandlineInfo {
  cliNumbers: [CliNumber]
}

enum Language {
  EN
  DE
  FR
  IT
}

type LinkedSlaveRatePlan {
  ratePlanName: String!
  serviceSerial: String!
}

scalar LocalDate

scalar LocalDateTime

input MakePpaAgreementInput {
  invoiceIds: [String]
  sapDocumentIds: [String]
  invoicesAmount: Float!
  numberOfInstallments: Int
}

type MakePpaAgreementPayload {
  payloadStatus: PayloadStatus!
}



type Option {
  id: String
  activationDate: LocalDate
  scheduledActivationDate: LocalDate
  conflictsWith: [String]
  conflictsWithInstalled: [String]
  dependsOn: String
  dependsOnNotInstalled: String
  earliestDeactivationDate: LocalDate
  installed: Boolean
  modifiable: Boolean
  scheduledDeactivationDate: LocalDate
  status: OptionStatus
  buckets: [Bucket]
  categories: [Category]
  pictureUrl: String
  factSheet: FactSheet
  oneLineTitle: String
  description: String
  info: String
  insurancePurchase: OptionInsurancePurchase
  activationInfo: String
  deactivationInfo: String
  price: ProductPrice
  type: OptionType
  agbs: [Agb]
  inputConfig: [OptionInputConfig]
  autoRenewal: Boolean
}

input OptionAdditionalProperty {
  key: OptionAdditionalPropertyKey
  value: String
}

enum OptionAdditionalPropertyKey {
  LOGIN_NAME
  CONTACT_NUMBER
}

type OptionInputConfig {
  id: String
}

enum OptionInstalled {
  INSTALLED
  AVAILABLE
  ALL
}

type OptionInsurancePurchase {
  deviceDescription: String
  imei: String
  devicePrice: Float
  purchaseDate: LocalDate
  purchasePlace: String
  price: Float
}

enum OptionStatus {
  RECURRING
  EXPIRING
  SUSPENDED
  WAITING
  UNINSTALLED
  PENDING_ONEDRIVE_PROVISIONING
  PENDING_ONEDRIVE_CEASE
}

enum OptionType {
  SERVICE
  INSURANCE
}

input OrderAccountStatementInput {
  accountStatementMethod: AccountStatementMethod!
}

type OrderAccountStatementPayload {
  payloadStatus: PayloadStatus
}

input OrderAdditionalPaymentSlipInput {
  amount: Float
}

type OrderAdditionalPaymentSlipPayload {
  payloadStatus: PayloadStatus!
}

input OrderBillCopyInput {
  billId: String
}

type OrderBillCopyPayload {
  payloadStatus: PayloadStatus
}

input OrderPpaBillCopyInput {
  sapDocumentId: String
}

type OrderPpaBillCopyPayload {
  payloadStatus: PayloadStatus
}

enum PayloadStatus {
  OK
  NOK
}

type PaymentAmount {
  amount: Float
  dueDate: LocalDate
  documentTypes: [BillDocumentType]
  type: PaymentAmountType
}

enum PaymentAmountType {
  OVERDUE
  CURRENT
  FUTURE
}

enum PaymentCard {
  ECA
  VIS
  AMX
  PEF
  PFC
}

input PaymentInput {
  amount: Float!
  paymentTransaction: PaymentTransaction!
}

enum PaymentMethodOption {
  CREDIT_CARD
  REGISTERED_CARD
  E_BANKING
  PAYMENT_SLIP
  PAYMENT_SERVICE
}

enum PaymentMethodType {
  PAY_SLIP
  DIRECT_DEBIT
  EBILL
}

input PaymentMethodTypeInput {
  paymentMethodType: PaymentMethodType!
}

type PaymentMethodTypePayload {
  payloadStatus: PayloadStatus
}

type PaymentPayload {
  payloadStatus: PayloadStatus!
}

type PaymentProvider {
  providerUrl: String!
  merchantId: String!
  amount: Float!
  currency: String!
  referenceNumber: String!
  sign: String!
  uppReturnMaskedCc: String
  paymentCards: [PaymentCard!]
  paymentServices: [PaymentServiceType!]
}

type PaymentRequest {
  accountId: String!
  date: LocalDateTime!
  amount: Float!
  totalOutstandingAmount: Float!
  transactionId: String!
  number: String
  paymentCard: PaymentCard
  paymentService: PaymentServiceType
}

enum PaymentServiceType {
  PAP
  TWI
}

type PaymentState {
  paymentMethods: [PaymentMethodOption]
  paymentProvider: PaymentProvider
  registeredCards: [RegisteredCard]
}

input PaymentTransaction {
  acqAuthorizationCode: String!
  authorizationCode: String!
  uppTransactionId: String!
  merchantId: String!
  paymentCard: PaymentCard
  paymentService: PaymentServiceType
  useAlias: Boolean
  aliasCc: String
  expiryMonth: String
  expiryYear: String
  number: String
  savePayment: Boolean
}

enum PostpaidStatus {
  ACTIVE
  ACTIVE_INT_LOW
  INACTIVE
  SUSPENDED_BY_USER
  SUSPENDED_BY_OPERATOR
  DEACTIVE
  TERMINATED
}

type PpaEligibility {
  eligible: Boolean!
  maxAmount: Float
  minAmount: Float
  minInstallmentAmount: Int
  fee: Float
  minInstallments: Int
  maxInstallments: Int
}

type PpaInstallment {
  id: Int!
  fee: Float!
  amount: Float!
  amountTotal: Float!
  dueDate: LocalDate
}

type PpaInstallmentOption {
  count: Int!
  fee: Float!
  amount: Float!
  amountTotal: Float!
  installments: [PpaInstallment!]!
}

input PpaInstallmentOptionsInput {
  amount: Float!
}

enum PrepaidStatus {
  ACTIVE
  ACTIVE_INT_LOW
  INACTIVE
  SUSPENDED_BY_USER
  SUSPENDED_BY_OPERATOR
  DEACTIVE
  TERMINATED
}

type PrivacySetting {
  name: SettingType!
  value: Boolean!
  serviceSettings: [ServiceSetting!]
  log: PrivacySettingLog
}

input PrivacySettingInput {
  name: SettingType!
  value: Boolean!
  serviceSettings: [ServiceSettingInput!]
}

type PrivacySettingLog {
  actionDateTime: DateTime!
  channel: String!
  newValue: String
  previousValue: String
}

type Product {
  promotionInfos: [PromotionInfo]
}

type ProductDiscount {
  absolute: Boolean
  isPromotionDependent: Boolean
  name: String
  numberOfMonths: Int
  value: Float
}

type ProductDiscountedPrice {
  free: Boolean
  value: Float
}

type ProductOriginalPrice {
  free: Boolean
  value: Float
}

type ProductPrice {
  priceType: String
  originalPrice: ProductOriginalPrice
  discountedPrice: ProductDiscountedPrice
  billingTime: BillingTime
  discounts: [ProductDiscount]
  currencyIso: String
  recurring: Recurring
}

type PromotionInfo {
  promoId: String
  type: PromotionType
  targetedTypes: [TargetedType]
}

enum PromotionType {
  BOTH
  TARGET
  QUALIFIER
}


type RatePlan {
  name: String!
  oneLineTitle: String
  description: String
  info: String
  type: RatePlanType
  fixnetInfo: FixnetInfo
  tvInfo: TvInfo
  tvAppInfo: TvAppInfo
  landlineInfo: LandlineInfo
  ongoingCost: Float
  buckets(serviceId: String, dataSpeedExtraSupport: Boolean): [Bucket]
  entitlements: [Entitlement]
  lifecycle: RatePlanLifecycle!
  factSheet: FactSheet
  categories: [Category]
  agbs: [Agb]
  cliNumber: String
  price: ProductPrice
  fee: Fee
  options(
    installed: OptionInstalled
    includeForRatePlanTypes: [RatePlanType]
  ): [Option]
  xofRatePlanInfo: XofRatePlanInfo
  status: Status
  product: Product
  group: String
  subGroup: String
  onpOrder: Boolean
  pendingText: String
}

enum RatePlanLifecycle {
  ACTIVE
  OBSOLETE
}

enum RatePlanType {
  MOBILE_PREPAID
  MOBILE_POSTPAID
  TV
  LANDLINE_PHONE
  FIX_INTERNET
  MOBILE_COVERAGE
  OTT_TV
  UNDEFINED
}

type Recurring {
  duration: Int
  period: RecurringPeriod
}

enum RecurringPeriod {
  HOUR
  DAY
  MONTH
}

input RegisterCardInput {
  registeredCard: RegisteredCardInput!
}

type RegisterCardPayload {
  payloadStatus: PayloadStatus!
}

type RegisteredCard {
  aliasCc: String!
  expiryMonth: String!
  expiryYear: String!
  number: String!
  paymentCard: PaymentCard!
}

input RegisteredCardInput {
  aliasCc: String!
  expiryMonth: String!
  expiryYear: String!
  number: String!
  paymentCard: PaymentCard!
}

type RelocationInfo {
  date: LocalDate
  dateOverdue: Boolean
  done: Boolean
  address: Address
}

type RequestDunningDeblockOnTrustPayLoad {
  payloadStatus: PayloadStatus!
}

type RequestDunningJokerPayLoad {
  payloadStatus: PayloadStatus!
}

type RetentionOffer {
  priority: Int!
  creationDate: DateTime
  expiryDate: DateTime
  destinationProducts: [DestinationProduct]
  discounts: [RetentionOfferDiscount]
  options: [RetentionOfferOption]
  devicePlans: [RetentionOfferDevicePlan]
  accessoryPlans: [RetentionOfferAccessoryPlan]
  creditNotes: [RetentionOfferCreditNote]
}

type RetentionOfferAccessoryPlan {
  accessoryPlan: HardwarePlan!
  action: RetentionOfferItemAction!
}

type RetentionOfferCreditNote {
  name: String!
  price: Float!
  action: RetentionOfferItemAction!
}

type RetentionOfferDevicePlan {
  devicePlan: HardwarePlan!
  action: RetentionOfferItemAction!
  pendingAmount: Float
  contractBinding: Boolean
  contractDuration: Int
}

type RetentionOfferDiscount {
  name: String!
  serviceDuration: Int
  price: Float
  percentage: Float
  action: RetentionOfferItemAction!
  discountedPrice: Float
}

enum RetentionOfferItemAction {
  ADDED
  DELETED
  CONTINUED
}

type RetentionOfferOption {
  name: String!
  serviceDuration: Int
  price: Float
  action: RetentionOfferItemAction!
}

type RetentionProduct {
  retentionOffers: [RetentionOffer]!
  serial: String!
  name: String
  price: Float
  ceaseReason: String
}

enum SalutationType {
  MR
  MS
  HERR
}

enum ScanIdStatus {
  ID_REQUIRED
  ID_SCANNED
  ID_EXPIRED
  UID_CHECKED
  ON_TRUST
  BC_EXCEPTION
  BC_GOVERNANCE
  ID_SCANNED_APPROVAL_PENDING
}

enum Segment {
  LARGE_CORPORATE
  MEDIUM_CORPORATE
  SMALL_CORPORATE
  SOHO
  RESIDENTIAL
  EMPLOYEE
  UNDEFINED
}

type SendNameChangeInfoPayload {
  payloadStatus: PayloadStatus!
}

type SendVerificationEmailPayload {
  payloadStatus: PayloadStatus!
}

type Service {
  id: ID!
  name: String
  ratePlans: [RatePlan]!
  subscriber: Subscriber
  balance: Float
  contract: Contract
  otoId: String
  installationAddress: Address
  unverifiedOtoId: String
  simCard: SimCard
  slaveServices(slaveServiceId: String): [SlaveService]
  merge: Boolean!
  billingMethod: ServiceBillingMethod
  retentionProduct: RetentionProduct
  prepaidStatus: PrepaidStatus
  postpaidStatus: PostpaidStatus
  fixnetStatus: FixnetStatus
  hardware: Hardware
  xofProgramInfo: XofServiceProgramInfo
  xofActiveSubscription: XofActiveSubscription
  status: Status
}

enum ServiceBillingMethod {
  INVOICE
  CREDIT_CARD
}

type ServiceSetting {
  serviceId: String!
  attributes: [ServiceSettingAttribute!]
}

type ServiceSettingAttribute {
  name: ServiceSettingType!
  value: Boolean!
}

input ServiceSettingAttributeInput {
  name: ServiceSettingType!
  value: Boolean!
}

input ServiceSettingInput {
  serviceId: String!
  attributes: [ServiceSettingAttributeInput!]!
}

enum ServiceSettingType {
  SMS_ALLOWED
}

input SetBillDeliveryMethodInput {
  billDeliveryMethod: BillDeliveryMethod!
  billDeliveryNotification: BillDeliveryNotificationInput
}

type SetBillDeliveryMethodPayload {
  payloadStatus: PayloadStatus
}

input SetBillDeliveryNotificationInput {
  contactNumbers: [String]!
}

type SetBillDeliveryNotificationPayload {
  payloadStatus: PayloadStatus
}

input SetBillingAddressInput {
  sameAsCustomerAddress: Boolean
  differentBillingAddress: AddressInput
}

type SetBillingAddressPayload {
  payloadStatus: PayloadStatus
}

input SetSiteLevelOptionInput {
  productId: String!
  activate: Boolean!
  category: SiteLevelOptionCategory!
}

type SetSiteLevelOptionPayload {
  payloadStatus: PayloadStatus!
}

enum SettingType {
  GENERAL_MARKETING
  CHANNEL_SMS
  CHANEL_CALL
  CHANNEL_LETTER
  CHANNEL_WEBAPP
  CHANNEL_MESSENGER
  SERVICES_AND_TECHNOLOGY
  TV_RECOMMENDATION
  PROMOTION_AND_SERVICES
  PROFILE_ANALYSIS
  PROFILE_OFFER
  PROCESSING_SMART_DATA
  MULTISOURCE_ADDRESS_USAGE
}

type SimCard {
  imsi: String
  iccid: String
  status: SimCardStatus
}

enum SimCardStatus {
  INSTALLED
  SUSPENDED
  DISCONNECTED
}

type SiteLevelOption {
  id: String
  activationDate: LocalDate
  earliestDeactivationDate: LocalDate
  modifiable: Boolean
  status: SiteLevelOptionStatus
  minDuration: Int
  serviceDuration: Int
  oneLineTitle: String
  price: SiteLevelOptionPrice
  name: String
  description: String
  category: SiteLevelOptionCategory
  surfProtectFamilyItems: [SurfProtectFamilyItem]
}

enum SiteLevelOptionCategory {
  PREMIUM
  FAMILY
  SURF_PROTECT_FAMILY
}

type SiteLevelOptionPrice {
  originalPrice: Float
  discountName: String
  discountAmount: Float
  discountedPrice: Float
}

enum SiteLevelOptionStatus {
  INSTALLED
  DEINSTALLED
  NOT_AVAILABLE
  PENDING_CANCELLATION
}

type SlaveRatePlan {
  name: String!
  oneLineTitle: String
  description: String
  info: String
  type: RatePlanType
  ongoingCost: Float
  entitlements: [Entitlement]
  lifecycle: RatePlanLifecycle!
  factSheet: FactSheet
  categories: [Category]
  agbs: [Agb]
  price: ProductPrice
  fee: Fee
  xofRatePlanInfo: XofRatePlanInfo
}

type SlaveService {
  id: ID!
  name: String
  subscriber: Subscriber
  contract: Contract
  simCard: SimCard
  ratePlans: [SlaveRatePlan]!
  xofProgramInfo: XofServiceProgramInfo
  xofActiveSubscription: XofActiveSubscription
}

enum Status {
  INSTALLED
  SUSPENDED
  TERMINATED
  PENDING
}

input SubmitEtfRefundFormInput {
  action: UploadAction
  files: [Upload!]!
}

type SubmitEtfRefundFormPayload {
  status: UploadActionStatus!
}

type Subscriber {
  firstName: String
  lastName: String
  name: String
  email: String
  emailConfirmed: Boolean
  birthDate: LocalDate
  phone: String
  language: Language
  companyName: String
  salutation: SalutationType
}

enum SubscriberType {
  SAME_AS_OWNER
  DIFFERENT_FROM_OWNER
}

type SurfProtectFamilyItem {
  ratePlanName: String!
  productId: String
  serviceSerial: String!
  phoneNumbers: [String]
  activated: Boolean!
  linkedSlaveRatePlans: [LinkedSlaveRatePlan]
}

enum TargetedType {
  EXISTING_CUSTOMER
  EXISTING_RP
  NEW_CUSTOMER
  NEW_RP
  MIGRATION
  ALL
  PRE_TO_POST
}

type TvAppInfo {
  serviceName: String
  loginName: String
  language: Language
  subscriberId: String
  providerName: String
}

type TvInfo {
  includedServices: [Entitlement!]!
}

input UpdateContactInput {
  email: String
  languageCode: Language
  phone: String
  firstName: String
  lastName: String
  birthDate: LocalDate
  salutation: SalutationType
  emailConfirmed: Boolean
}

type UpdateContactPayload {
  payloadStatus: PayloadStatus!
}

input UpdatePasswordInput {
  newPassword: String!
  oldPassword: String!
}

type UpdatePasswordPayload {
  payloadStatus: PayloadStatus!
}

input UpdatePrivacySettingsInput {
  privacySettings: [PrivacySettingInput!]!
}

type UpdatePrivacySettingsPayload {
  payloadStatus: PayloadStatus!
}

type UpdateSinglePrivacySettingPayload {
  payloadStatus: PayloadStatus!
}

input UpdateSubscriberInput {
  serviceId: String!
  contact: UpdateContactInput!
  subscriberType: SubscriberType!
}

type UpdateSubscriberPayload {
  payloadStatus: PayloadStatus
}

input UpdateSurfProtectFamilyInput {
  productId: String
  serviceSerial: String
  activated: Boolean
}

type UpdateSurfProtectFamilyPayload {
  payloadStatus: PayloadStatus
}

scalar Upload

input UploadAction {
  type: UploadActionType!
  parameters: [UploadActionParameter]
}

input UploadActionParameter {
  key: UploadActionParameterType!
  value: String!
}

enum UploadActionParameterType {
  SERVICE_ID
  ETF_AMOUNT
}

type UploadActionStatus {
  actionId: String
  type: UploadActionType
}

enum UploadActionType {
  ETF_REFUND
}

type XofAccountDiscountInfo {
  allowedDiscounts: Int
  remainingDiscounts: Int
  duration: Int
  newExpiryDate: LocalDate
  discountPercentage: Float
  type: String
  renewalDuration: Int
}

type XofAccountProgram {
  accountInfo: XofProgramAccountInfo
  status: XofAccountProgramStatus
}

type XofAccountProgramStatus {
  activationStatus: XofStatus
  type: FirmProgramType
}

type XofActiveSubscription {
  status: XofSubscriptionStatus
  discountStatus: XofSubscriptionDiscountStatus
  discountStartDate: LocalDate
  discountEndDate: LocalDate
  discountPercentage: Float
}

enum XofEmailValidationType {
  BUSINESS_EMAIL
  SCAN_UPLOAD
}

type XofProgramAccountInfo {
  email: String
  companyName: String
  companyId: String
  discountInfo: XofAccountDiscountInfo
}

type XofRatePlanInfo {
  isActive: Boolean
  discountPercentage: Float
}

type XofServiceProgramInfo {
  companyName: String
  discount: Float
  duration: Int
  contractDuration: Int
  renewalDuration: Int
}

enum XofStatus {
  ACTIVE
  INACTIVE
  IN_PROGRESS
}

enum XofSubscriptionDiscountStatus {
  INSTALLED
  NOT_INSTALLED
}

enum XofSubscriptionStatus {
  ACTIVE
  IN_PROGRESS
}

type Query {
  account(language: Language, extendedBlocked: Boolean): Account
  countries(language: Language): [Country]
  paymentState(amount: Float!): PaymentState!
  paymentRequests: [PaymentRequest!]
  emailValidationState(email: String!): EmailValidationState!
  addressValidity(input: AddressValidityInput!): AddressValidity!
  ppaInstallmentOptions(
    input: PpaInstallmentOptionsInput!
  ): [PpaInstallmentOption!]
  addressSuggestion(input: AddressSuggestionInput!): AddressSuggestionPayload
  subscriber(country: String, language: Language): RoamingSubscriber
  validateOrderOption(
    method: RoamingPaymentMethod!
    optionId: String!
  ): PaymentValidation!
  validateRefill(
    amount: Float!
    ccNumber: String
    method: RoamingPaymentMethod!
    msisdn: String!
  ): PaymentValidation!
}

type Mutation {
  setBillingAddress(input: SetBillingAddressInput!): SetBillingAddressPayload
  setBillAnonymizationLevel(
    input: BillAnonymizationLevelInput!
  ): BillAnonymizationLevelPayload
  orderAccountStatement(
    input: OrderAccountStatementInput!
  ): OrderAccountStatementPayload
  orderBillCopy(input: OrderBillCopyInput!): OrderBillCopyPayload
  orderPpaBillCopy(input: OrderPpaBillCopyInput!): OrderPpaBillCopyPayload
  setBillDeliveryMethod(
    input: SetBillDeliveryMethodInput!
  ): SetBillDeliveryMethodPayload
  setBillDeliveryNotification(
    input: SetBillDeliveryNotificationInput!
  ): SetBillDeliveryNotificationPayload
  setPaymentMethod(input: PaymentMethodTypeInput!): PaymentMethodTypePayload
  orderAdditionalPaymentSlip(
    input: OrderAdditionalPaymentSlipInput
  ): OrderAdditionalPaymentSlipPayload
  makePayment(input: PaymentInput!): PaymentPayload
  registerCard(input: RegisterCardInput!): RegisterCardPayload
  deleteRegisteredCard(
    input: DeleteRegisteredCardInput!
  ): DeleteRegisteredCardPayload
  requestDunningJoker: RequestDunningJokerPayLoad
  requestDunningDeblockOnTrust: RequestDunningDeblockOnTrustPayLoad
  sendVerificationEmail: SendVerificationEmailPayload!
  updateContact(input: UpdateContactInput): UpdateContactPayload!
  updatePassword(input: UpdatePasswordInput!): UpdatePasswordPayload!
  sendNameChangeInfo: SendNameChangeInfoPayload!
  makePpaAgreement(input: MakePpaAgreementInput!): MakePpaAgreementPayload!
  editOptions(input: EditOptionsInput!): EditOptionsPayload!
  updateSinglePrivacySetting: UpdateSinglePrivacySettingPayload!
  updatePrivacySettings(
    input: UpdatePrivacySettingsInput!
  ): UpdatePrivacySettingsPayload!
  acceptRetentionOffer(
    input: AcceptRetentionOfferInput!
  ): AcceptRetentionOfferPayload!
  createXofProgram(serviceId: String!): CreateXofProgramPayload!
  enrollXofProgram(input: EnrollXofProgramInput!): EnrollXofProgramPayload!
  submitEtfRefundForm(
    input: SubmitEtfRefundFormInput!
  ): SubmitEtfRefundFormPayload!
  setSiteLevelOption(
    input: SetSiteLevelOptionInput!
  ): SetSiteLevelOptionPayload!
  updateSurfProtectFamily(
    input: [UpdateSurfProtectFamilyInput]!
  ): UpdateSurfProtectFamilyPayload

  activateSiteLevelOption(
    input: ActivateSiteLevelOptionInput!
  ): ActivateSiteLevelOptionPayload!
  confirmRefill(input: ConfirmRefillInput!): ConfirmRefillPayload!
  deactivateOption(input: DeactivateOptionInput!): DeactivateOptionPayload!
  deactivateSiteLevelOption(
    input: DeactivateSiteLevelOptionInput!
  ): DeactivateSiteLevelOptionPayload!
  orderOption(input: OrderOptionInput!): OrderOptionPayload!
  readNotification(input: ReadNotificationInput!): ReadNotificationPayload!
  rescheduleOption(input: RescheduleOptionInput!): RescheduleOptionPayload!
  updateSetting(input: UpdateSettingInput!): UpdateSettingPayload!
}
`;

const CockPitSchemaDef=`
type RoamingAccount {
  id: ID!
  options(category: RoamingSiteLevelOptionCategory): [RoamingSiteLevelOption]
}

input ActivateSiteLevelOptionInput {
  category: RoamingSiteLevelOptionCategory!
  productId: String!
}

type ActivateSiteLevelOptionPayload {
  status: PayloadStatus!
}

type RoamingBucket {
  activatedAt: DateTime
  active: Boolean
  available: Int
  credit: RoamingBucketCredit!
  expiresAt: DateTime
  initial: Int!
  measureUnit: RoamingBucketMeasureUnit!
  name: String
  title: String
  traffic: BucketTraffic
  type: RoamingBucketType!
}

enum RoamingBucketCredit {
  DATA
  MMS
  SMS
  VOICE
  WHATSAPP
  WIFI
}

enum RoamingBucketMeasureUnit {
  GB
  KB
  MB
  MINUTE
  MMS
  SECOND
  SMS
}

type BucketSummary {
  available: Int
  credit: RoamingBucketCredit!
  initial: Int
  measureUnit: RoamingBucketMeasureUnit!
  type: RoamingBucketType!
}

enum BucketTraffic {
  IN
  IN_OUT
  OUT
}

enum RoamingBucketType {
  LIMITED
  THROTTLED
  UNLIMITED
}

type CatalogOption {
  conflictsWith: [String!]
  countries: [String!]
  dependsOn: String
  description: String
  displayPriority: Int
  extendBy: String
  header: String
  id: String!
  name: String!
  price: Float
  promo: Boolean!
  regions: [RoamingRegion!]
  subTitle: String
  title: String
  type: RoamingOptionType!
}

input ConfirmRefillInput {
  amount: Float!
  method: RoamingPaymentMethod!
  msisdn: String!
  notification: String
  registeredCard: String
  transaction: TransactionDetails
}

type ConfirmRefillPayload {
  amount: Float
  method: RoamingPaymentMethod
  msisdn: String
  status: PayloadStatus!
}

type RoamingCountry {
  code: String!
  dataRoamingAvailable: Boolean!
  name: String!
  regions: [RoamingRegion!]
  roamingPartnerAvailable: Boolean!
}


input DeactivateOptionInput {
  id: String!
}

type DeactivateOptionPayload {
  status: PayloadStatus!
}

input DeactivateSiteLevelOptionInput {
  category: RoamingSiteLevelOptionCategory!
  productId: String!
}

type DeactivateSiteLevelOptionPayload {
  status: PayloadStatus!
}

input EmailTransactionDetails {
  email: String!
  firstName: String!
  lastName: String!
  transaction: TransactionDetails!
}


type GeoLocation {
  country: RoamingCountry!
  timestamp: String
}


type Notification {
  cta: String
  description: String!
  id: String!
  mandatory: Boolean!
}

type RoamingOption {
  activatedAt: DateTime
  buckets: [RoamingBucket!]
  conflictsWith: [String!]
  countries: [String!]
  dependsOn: String
  description: String
  displayPriority: Int
  earliestDeactivationAt: DateTime
  expiresAt: DateTime
  extendBy: String
  factSheet: FactSheet
  header: String
  id: String!
  inConflict: Boolean!
  modifiable: Boolean
  name: String!
  price: Float
  promo: Boolean!
  purchasedAt: DateTime
  regions: [RoamingRegion!]
  remainingDataState: RemainingState!
  status: RoamingOptionStatus
  subTitle: String
  title: String
  type: RoamingOptionType!
  visible: Boolean!
}

enum RoamingOptionStatus {
  ACTIVE
  AVAILABLE
  EXPIRED
  INSTALLED
  PENDING
  RECURRING
}

enum RoamingOptionType {
  DATA
  DAYS
  DAY_PASS
  OTHER
  RATE_PLAN
  RECURRING
  SITE
  VOICE
}

input OrderOptionInput {
  activationDate: DateTime
  id: String!
  payment: EmailTransactionDetails
}

type OrderOptionPayload {
  status: PayloadStatus!
}


type Payment {
  group: PaymentGroup!
  method: RoamingPaymentMethod!
  registeredCards: [RegisteredCardDetails!]
}

enum PaymentCardType {
  AMX
  ECA
  PEF
  PFC
  VIS
}

enum PaymentGroup {
  OPTION
  REFILL
}

enum RoamingPaymentMethod {
  CREDIT_CARD
  INVOICE
  POST_CARD
  REGISTERED_CARD
}

type RoamingPaymentProvider {
  alias: String
  amount: Int!
  currency: String!
  expiryMonth: String
  expiryYear: String
  merchantId: String!
  paymentMethods: [PaymentCardType!]
  providerUrl: String!
  refNo: String!
  sign: String!
  uppReturnMaskedCc: String
  useAlias: String
}

type PaymentValidation {
  provider: RoamingPaymentProvider
  status: ValidationStatus!
}

input ReadNotificationInput {
  id: String!
}

type ReadNotificationPayload {
  status: PayloadStatus!
}

type RegionStatus {
  autoInstalled: [CatalogOption!]
  countries: [String!]
  futureOptions: [RoamingOption!]
  options: [RoamingOption!]
  region: RoamingRegion
  summaries: [BucketSummary!]
}

type RegisteredCardDetails {
  number: String!
}

enum RemainingState {
  FULL
  LOW
  NA
  NONE
  OK
}

input RescheduleOptionInput {
  activationDate: DateTime!
  id: String!
}

type RescheduleOptionPayload {
  status: PayloadStatus!
}

enum RoamingRegion {
  ALL
  BORDERS
  EUROPE
  ONE
  ONE_PLUS
  THREE
  TWO
  UNLIMITED_36
  USA_CANADA
  WORLD
  ZERO
  ZONE_A
  ZONE_ABCD
  ZONE_A_MINUS
  ZONE_B
  ZONE_C
  ZONE_D
  ZONE_E
}

enum RoamingSegment {
  LARGE_CORPORATE
  MEDIUM_CORPORATE
  RESIDENTIAL
  SMALL_CORPORATE
  SOHO
}

type ServicePlan {
  balance: Float
  name: String!
  type: ServiceType!
}

enum ServiceType {
  POSTPAID
  PREPAID
}

type Setting {
  modifiable: Boolean!
  value: String!
}

type Settings {
  autoInstallRoamingOptions: Setting
  callsAbroad: Setting
  cost: Float
  monthlyLimit: Setting
  noDataRoamingOutsideBucket: Setting
  noDataRoamingOutsideBucketPre: Setting
  redirectToMailBoxAbroad: Setting
  roamingInfoSms: Setting
  seaAndAirSatelliteTraffic: Setting
}

type RoamingSiteLevelOption {
  activationDate: DateTime
  category: RoamingSiteLevelOptionCategory!
  countries: [String!]
  description: String
  earliestDeactivationDate: DateTime
  header: String
  id: String!
  modifiable: Boolean!
  name: String!
  price: SiteLevelOptionPrice
  regions: [RoamingRegion!]
  status: RoamingSiteLevelOptionStatus!
  subTitle: String
  title: String
}

enum RoamingSiteLevelOptionCategory {
  FAMILY
}


enum RoamingSiteLevelOptionStatus {
  DEINSTALLED
  INSTALLED
  PENDING_CANCELLATION
}

type RoamingSubscriber {
  allowed: Boolean
  blocked: Boolean
  companyName: String
  location: GeoLocation
  msisdn: String!
  notifications: [Notification!]
  option(id: String!): RoamingOption
  options: [RoamingOption!]
  payments(group: PaymentGroup): [Payment!]
  segment: RoamingSegment!
  service: ServicePlan!
  settings: Settings
  status: [RegionStatus!]!
}

input TransactionDetails {
  acqAuthCode: String
  alias: String
  authAmount: String
  authCode: String
  ccTransactionId: String
  expiryMonth: String
  expiryYear: String
  merchantId: String
  number: String
  paymentMethod: PaymentCardType
}

input UpdateSettingInput {
  key: String!
  value: String!
}

type UpdateSettingPayload {
  setting: Setting!
}

enum ValidationStatus {
  NOK
  OK
}
`
module.exports = { SchemaDef, CockPitSchemaDef } 