const { ApolloServer } = require('apollo-server');
const { makeExecutableSchema } = require('graphql-tools');
const {contactInput}= require('./Data/MutationPayload/contactInput'); 
// A schema is a collection of type definitions (hence "typeDefs")
// that together define the "shape" of queries that are executed against
// your data.

const { SchemaDef, CockPitSchemaDef } = require('./schema'),
    MockData = require('./Data/data.js'),
    data = MockData.data;

const resolvers = {
  Query: {
    account: (language, forceUpdate) => data.account,
    countries: (language) => data.countries,
    addressValidity: (input) => data.addressValidity,
    paymentState: (amount) => data.paymentState,
    paymentRequests: () => data.paymentRequests,
    subscriber: (country, language) => data.roamingOptions,
    emailValidationState:(email, forceUpdate) => data.emailValidation
  },
  Mutation: {
    acceptRetentionOffer: (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    setSiteLevelOption: (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    updateContact: (UpdateContactInput) => {
      return ({
            "payloadStatus": "OK",
            "__typename": "UpdateContactPayload"}
        )
    },
    setBillingAddress :  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    setBillAnonymizationLevel:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    orderAccountStatement:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    orderBillCopy:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    orderPpaBillCopy:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    setBillDeliveryMethod:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    setBillDeliveryNotification:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    setPaymentMethod:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    orderAdditionalPaymentSlip:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    makePayment:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    registerCard:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    deleteRegisteredCard:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    requestDunningJoker:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    requestDunningDeblockOnTrust:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    sendVerificationEmail:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    updateContact:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    updatePassword:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    sendNameChangeInfo:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    makePpaAgreement:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    editOptions:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    updateSinglePrivacySetting:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    updatePrivacySettings:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    createXofProgram:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    enrollXofProgram:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    submitEtfRefundForm:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    updateSurfProtectFamily:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    activateSiteLevelOption:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    confirmRefill:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    deactivateOption:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    deactivateSiteLevelOption:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    orderOption:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    readNotification:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    rescheduleOption:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
    updateSetting:  (input) => {
      return ({
        "payloadStatus": "OK"
      })
    },
  }
};


const schema = makeExecutableSchema({
  typeDefs: [SchemaDef, CockPitSchemaDef],
  resolvers: resolvers
});


const server = new ApolloServer({ schema, uploads: false });
const PORT = 3005;
// The `listen` method launches a web server.
server.listen({ port: PORT }).then(({ url }) => {
  console.log(`??  Server ready at ${url}`);
});
